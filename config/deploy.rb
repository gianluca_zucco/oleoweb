#lock '3.6.1'

set :application, 'oleoweb'
set :repo_url, 'git@bitbucket.org:gianluca_zucco/oleoweb.git'
set :scm, :git
set :keep_releases, 2
