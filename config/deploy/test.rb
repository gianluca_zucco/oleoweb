set :branch, 'development'
set :deploy_to, '/home/oleoweb/test'
server '194.209.228.250', user: 'root', roles: %w{app db web}
append :linked_files, 'htdocs/app/etc/local.xml'
append :linked_dirs, 'htdocs/media', 'htdocs/var'
