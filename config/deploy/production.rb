set :branch, :master
set :deploy_to, '/home/oleoweb/production'
server '194.209.228.250', user: 'root', roles: %w{app db web}
append :linked_files, 'htdocs/app/etc/local.xml', 'htdocs/robots.txt'
append :linked_dirs, 'htdocs/media', 'htdocs/var'
