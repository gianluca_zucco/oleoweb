var MzSlider;
MzSlider = Class.create(MzWidget, {
    evaluateSelects: function () {
        var selects = $$('.mzentrale_widget.select');
        for (var k in selects) {
            if (!selects[k].id) continue;
            var splittedId = selects[k].id.split('_');
            var currentId = splittedId[0];
            splittedId.shift();
            var fieldId = splittedId.join('_');
            var currentValue = this.existing[currentId][fieldId];
            selects[k].select('option[value="' + currentValue + '"]')[0].writeAttribute('selected', true);
        }
    },
    initializeExisting: function () {
        //Initialize existing object
        if (Object.keys(this.existing).length > 0) {
            for (var objProp in this.existing) {
                this.addElement('slider-slide-template', this.existing[objProp], objProp);
                //Initialize accordion
                $(objProp).writeAttribute('style', 'display: none');
            }
        }
        this.evaluateSelects();
        Event.fire(document, 'existing:added')
    },
    getTemplateId: function () {
        return 'slider-slide-template';
    }
});
