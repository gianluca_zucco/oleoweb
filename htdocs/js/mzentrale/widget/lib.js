var MzWidget;
MzWidget = Class.create({
    config: {
        sorter_container_id: 'fieldsets'
    },
    evaluateSelects: function () {
        var selects = $$('.mzentrale_widget.select');
        for (var k in selects) {
            if (!selects[k].id) continue;
            var splittedId = selects[k].id.split('_');
            var idx = /[\d]+/.exec(selects[k].id).index;
            var offset = /[\d]+/.exec(selects[k].id)[0];
            var currentId = splittedId[0];
            currentId = selects[k].id.substring(0, idx + offset.length);
            var fieldId = selects[k].id.substring(idx + offset.length + 1);
            var currentValue = this._flattenize(this.existing[currentId], {})[fieldId];
            var selectedOption = selects[k].select('option[value="' + currentValue + '"]')[0];
            if (selectedOption)   {
                selectedOption.writeAttribute('selected', true);
            }
        }
    },
    initializeExisting: function () {
        //Initialize existing object
        if (Object.keys(this.existing).length > 0) {
            for (var objProp in this.existing) {
                var templateId = /([A-Za-z_]+)/.exec(objProp)[0].slice(0, -1);
                if ($(templateId))  {
                    this.addElement(templateId, this.existing[objProp], objProp);
                    //Initialize accordion
                    $(objProp).writeAttribute('style', 'display: none');
                } else {
                    console.log(templateId + " is not defined");
                }
            }
        }
        this.evaluateSelects();
        Event.fire(document, 'existing:added')
    },
    initialize: function () {
        try {
            this.existing = arguments[0];
            this.sorter = Sortable.create(this.config.sorter_container_id);
            this.initializeExisting();
        } catch (exception) {
            console.log(exception.message);
        }
    },
    createRowElement: function(element)
    {
        var templateId = $F(element);
        element.selectedIndex = 0;
        var date = new Date;
        var containerId = templateId+'_'+date.getTime();
        this.addElement(templateId, {}, containerId);
    },
    getTemplateId: function () {
        return '';
    },
    _prepareHtml: function (html) {
        var fragment = document.createElement('div');
        fragment.innerHTML = html;
        //Add a fullwidth class to single fieldset in multiple enviroment
        if (fragment.select('.mzwidget.subfieldset').length == 1) {
            fragment.select('.mzwidget.subfieldset').each(function (element) {
                $(element).addClassName('fullwidth');
            });
        }
        html = fragment.innerHTML;
        return html;
    },
    addElement: function () {
        var templateId = arguments[0] ? arguments[0] : this.getTemplateId();
        var extend = arguments[1];
        var containerId = arguments[2];

        var container = $(this.config.sorter_container_id);
        var templateHtml = $(templateId).innerHTML;
        var template = new Template(templateHtml);
        if (!containerId)   {
            var date = new Date;
            containerId = date.getTime();
        }
        var object = {
            'legend': Translator.translate(templateId),
            'form-id': containerId,
            'field_prefix': containerId
        };

        //Extend base properties with saved ones
        for (var key in extend) {
            object[key] = extend[key];
        }

        object = this._flattenize(object, {});
        var html = template.evaluate(object);
        html = this._prepareHtml(html);
        container.insert(html);

        if ($(containerId+'_valid_from'))   {
            Calendar.setup({
                inputField: containerId+'_valid_from',
                ifFormat: "%d.%m.%Y",
                showsTime: false,
                button: containerId+'_valid_from_trigger',
                align: "Bl",
                singleClick : true
            });
        }

        if ($(containerId+'_valid_to')) {
            Calendar.setup({
                inputField: containerId+'_valid_to',
                ifFormat: "%d.%m.%Y",
                showsTime: false,
                button: containerId+'_valid_to_trigger',
                align: "Bl",
                singleClick : true
            });
        }
        this.sorter = Sortable.create(this.config.sorter_container_id);
    },
    _flattenize: function(object, flattened)    {
        for (var key in object) {
            if (typeof object[key] == 'object') {
                this._flattenize(object[key], flattened);
            } else {
                flattened[key] = object[key];
            }
        }
        return flattened;
    },
    removeElement: function (element) {
        var confirmed = window.confirm(Translator.translate('Are you sure?'));
        if (confirmed != true) return;
        $(element).up(2).remove();
    },
    addImage: function(url) {
        MediabrowserUtility.openDialog(url, null, null, null, {
            onClose: function() {
                var parentId = /target_element_id\/([\w\d]+)/.exec(url)[1];
                var target = $(parentId+'_demo');
                setTimeout(function() {
                    target.src = $F(parentId);
                    if (target.src) {
                        $(target).removeClassName('hidden');
                    }
                }, 1000)
            }
        });
    },
    _openChooser: function (uniqueId, targetUrl) {
        window.uniqueId = new WysiwygWidget.chooser(
            uniqueId,
            targetUrl,
            {
                buttons: {
                    close: Translator.translate('Close'),
                    open: Translator.translate('Select products')
                }
            }
        );
        window.uniqueId.choose();
    },
    _prepareCmsChooserTargetUrl: function (element, targetElement, uniqid) {
        return element.readAttribute('data-url') +'uniq_id/'+uniqid+'/';
    },
    _prepareCategoryChooserTargetUrl: function (element, targetElement, uniqid) {
        return element.readAttribute('data-url') +'uniq_id/'+uniqid+'/';
    },
    _prepareProductChooserTargetUrl: function (element, targetElement, uniqid) {
        var targetUrl = element.readAttribute('data-url');
        targetUrl += 'product_type_id/configurable/';
        targetUrl += 'uniq_id/'+uniqid+'/';
        targetUrl += 'store_id/'+$F('store_ids')+'/';
        var currentValue = $F(targetElement);
        if (currentValue) {
            currentValue = currentValue.split(',');
            targetUrl += '?';
            for (var k = 0; k < currentValue.length; k++) {
                targetUrl += 'selected_products[]=' + currentValue[k] + '&';
            }
            targetUrl = targetUrl.substr(0, targetUrl.length - 1);
        }
        return targetUrl;
    },
    _openChooser: function (uniqueId, targetUrl, targetElement) {

        window[uniqueId] = new WysiwygWidget.chooser(
            uniqueId,
            targetUrl,
            {
                buttons: {
                    close: Translator.translate('Close'),
                    open: Translator.translate('Please select')
                }
            }
        );

        window[uniqueId].close = window[uniqueId].close.wrap(function(parentCall) {
            parentCall();
            targetElement.fire('dialog:close');
        });

        window[uniqueId].choose();
    },
    /**
     * Retrieve grid object from table
     *
     * @param target
     * @returns varienGrid
     */
    getGridObject: function (target) {
        var tableId = $(target).up(3).id.split('_');
        tableId.pop();
        return window[tableId.join('_') + 'JsObject'];
    },
    _bindProductChooserOnCheckboxAction: function (targetElement, uniqueId) {
        document.observe('click', function (event) {
            var target = event.target;
            if (target.readAttribute('name') == 'in_products') {
                var currentValue = $F(targetElement) ? $F(targetElement).split(',') : [];
                var regex = new RegExp(/&?selected_products[a-zA-Z0-9%\[\]]{1,}=(\d+)/, "gi");
                var gridObject = mzwidget.getGridObject(target);
                //Execute only if it has the right parent, and only once
                if (new RegExp(uniqueId+'$').test($(target).up(9).id)) {
                    var occurences = gridObject.url.match(regex) ? gridObject.url.match(regex) : [];
                    var label = $(uniqueId + 'label');
                    //Clean from all occurrences
                    gridObject.url = gridObject.url.replace(regex, '');
                    //Cleaning cycle
                    occurences.each(function(item, index) {
                        occurences[index] = item.split('=')[1];
                    });
                    if (target.checked) {
                        currentValue.push($F(target));
                        targetElement.value = currentValue.join();
                        //Push current value
                        occurences.push($F(target));
                        label.update(currentValue.join());
                        //Url build
                        occurences.each(function(item) {
                            gridObject.url += '&selected_products[]='+item;
                        });
                    } else {
                        var newValues = [];
                        currentValue.each(function (item) {
                            if (item != target.value) {
                                newValues.push(item)
                            }
                        });
                        targetElement.value = newValues.join();
                        label.update(newValues.join());
                        //Url build
                        occurences.each(function(item) {
                            if (item != target.value) {
                                gridObject.url += '&selected_products[]='+item;
                            }
                        });
                    }
                    event.stopImmediatePropagation();
                }
            }
        });
    },
    openProductChooser: function(element)    {

        if (!$F('store_ids')) {
            alert(Translator.translate('Please select a store view first'));
            return;
        }

        var uniqueId = element.readAttribute('data-unique-id');
        var targetElement = $(uniqueId+'value');
        var targetUrl = this._prepareProductChooserTargetUrl(element, targetElement, uniqueId);
        this._openChooser(uniqueId, targetUrl, targetElement);
        this._bindProductChooserOnCheckboxAction(targetElement, uniqueId);
        targetElement.observe('dialog:close', function() {
            $(uniqueId+'transport').value = $(uniqueId+'label').innerHTML;
            event.stopImmediatePropagation();
        });
    },
    openCmsPageChooser: function(element)    {
        var uniqueId = element.readAttribute('data-unique-id');
        var targetElement = $(uniqueId+'value');
        var targetUrl = this._prepareCmsChooserTargetUrl(element, targetElement, uniqueId);
        this._openChooser(uniqueId, targetUrl, targetElement);
        targetElement.observe('dialog:close', function() {
            $(uniqueId+'transport').value = $(uniqueId+'label').innerHTML;
            event.stopImmediatePropagation();
        });
    },
    openCategoryChooser: function(element)    {
        var uniqueId = element.readAttribute('data-unique-id');
        var targetElement = $(uniqueId+'value');
        var targetUrl = this._prepareCategoryChooserTargetUrl(element, targetElement, uniqueId);
        this._openChooser(uniqueId, targetUrl, targetElement);
        targetElement.observe('dialog:close', function() {
            $(uniqueId+'transport').value = $(uniqueId+'label').innerHTML;
            event.stopImmediatePropagation();
        });
    },
    openLinkChooser: function(element)    {
        var uniqueId = element.readAttribute('data-unique-id');
        Dialog.info($(uniqueId+'_dialog_template').innerHTML, {
            draggable:true,
            resizable:true,
            closable:true,
            className:"magento",
            windowClassName:"popup-window",
            title: Translator.translate('Insert link'),
            top:50,
            width:950,
            height:100,
            zIndex:1000,
            recenterAuto:false,
            hideEffect:Element.hide,
            showEffect:Element.show,
            id: uniqueId+"_widget-chooser"
        });
        Event.observe(document, 'click', function(event) {
            if (event.target.id == uniqueId + '_trigger')   {
                var selectedValue = $F(uniqueId+'clone');
                if (selectedValue) {
                    $(uniqueId+'value').value = selectedValue;
                    $(uniqueId+'label').update(selectedValue);
                    Windows.close(uniqueId+"_widget-chooser");
                }
            }
        });
    },
    openImageDialog: function(element) {

        s = document.createElement('div');
        img = document.createElement('img');
        img.src = element.src;
        s.appendChild(img);
        Dialog.info(s.innerHTML, {
            draggable:true,
            resizable:true,
            closable:true,
            className:"magento",
            windowClassName:"popup-window",
            title: Translator.translate('Image'),
            top:50,
            width:950,
            height: 'auto',
            zIndex:1000,
            recenterAuto:false,
            hideEffect:Element.hide,
            showEffect:Element.show,
            id: "image-widget-magnifier"
        });
    },
    copyToStore: function(element, instanceId) {
        console.log(element);
    }
});
