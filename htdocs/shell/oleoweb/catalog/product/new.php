<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/abstract.php';

class Oleoweb_Shell_Catalog_Product_New extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        /** @var Mage_Core_Model_Store $store */
        foreach (Mage::app()->getStores() as $store)    {
            $info = Mage::getModel('core/app_emulation')->startEnvironmentEmulation($store->getId(), Mage_Core_Model_App_Area::AREA_ADMIN);
            $this->_setProducts();
            Mage::getModel('core/app_emulation')->stopEnvironmentEmulation($info);
        }


    }

    protected function _setProducts()
    {
        /** @var Mage_Catalog_Model_Category $targetCategory */
        $targetCategory = Mage::getModel('catalog/category')->load(Mage::getStoreConfig('oleoweb_catalog/config/new_product_category_id'));
        if (!$targetCategory->getId()) {
            return;
        }
        /** @var Mage_Catalog_Model_Category_Api $api */
        $api = Mage::getModel('catalog/category_api');
        $assignedProductIds = array_map(function ($data) {
            return $data['product_id'];
        }, $api->assignedProducts($targetCategory->getId()));

        //Reset the whole category
        array_walk($assignedProductIds, function ($productId) use ($api, $targetCategory) {
            $api->removeProduct($targetCategory->getId(), $productId);
        });

        Mage::getResourceModel('catalog/product_collection')
            ->addFieldToFilter('news_from_date',
                array(array('lteq' => Mage::getModel('core/date')->date('Y-m-d')), array('null' => true)))
            ->addFieldToFilter('news_to_date',
                array('gteq' => Mage::getModel('core/date')->date('Y-m-d')))->walk(function (
                Mage_Catalog_Model_Product $product
            ) use ($api, $targetCategory) {
                $api->assignProduct($targetCategory->getId(), $product->getId());
                echo sprintf("%s has been added to \"new\" category %s", $product->getSku(), PHP_EOL);
            });
    }
}
$class = new Oleoweb_Shell_Catalog_Product_New();
$class->run();
