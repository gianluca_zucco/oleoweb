<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/abstract.php';

class Oleoweb_Shell_Catalog_Import_Simples extends Mage_Shell_Abstract
{
    const OLEOWEB_CATALOG_IMPORT_LOG = 'oleoweb_catalog_import.log';
    const GROUP_SKU_MAP_INDEX = 0;
    const EXISTING_SIMPLE_SKU_MAP_INDEX = 1;
    const PRICE_MAP_INDEX = 3;
    const NEW_SIMPLE_SKU_MAP_INDEX = 2;
    const NET_WEIGHT_MAP_INDEX = 4;

    /**
     * Run script
     *
     */
    public function run()
    {
        try {
            $info = Mage::getModel('core/app_emulation')->startEnvironmentEmulation(Mage_Core_Model_App::ADMIN_STORE_ID,
                Mage_Core_Model_App_Area::AREA_ADMINHTML);
            $websiteIds = (array) Mage::getModel('core/website')->load('shop', 'code')->getId();
            foreach ($this->_getData() as $productData) {
                try {
                    $this->_duplicate($productData, $websiteIds);
                    fwrite(STDOUT,
                        sprintf('Added new product %s %s', $productData[self::NEW_SIMPLE_SKU_MAP_INDEX], PHP_EOL));
                } catch (Mage_Core_Exception $e) {
                    fwrite(STDERR, $e->getMessage() . PHP_EOL);
                } catch (Exception $e) {
                    fwrite(STDERR, $e->getMessage() . PHP_EOL);
                }
            }
            Mage::getModel('core/app_emulation')->stopEnvironmentEmulation($info);
        } catch (Exception $e) {
            fwrite(STDERR, $e->getMessage() . PHP_EOL);
            Mage::log($e->getMessage(), null, self::OLEOWEB_CATALOG_IMPORT_LOG, true);
            Mage::log($e->getTraceAsString(), null, self::OLEOWEB_CATALOG_IMPORT_LOG, true);
        }
    }

    /**
     * @return array
     */
    public function _getData()
    {
        $filepath = $this->_getFilePath();
        $io = new Varien_File_Csv();
        $data = $io->getData($filepath);
        if (!$data) {
            Mage::throwException('Given file is empty or unreadable');
        }

        array_shift($data);

        $data = array_filter($data, function ($item) {
            foreach ($item as $key => $value) {
                if (!$value) {
                    return false;
                }
            }
            return true;
        });

        return array_map(function ($item) {
            foreach ($item as $key => &$value) {
                switch ($key) {
                    case self::PRICE_MAP_INDEX:
                        $value = str_replace(array(
                            '€',
                            ' ',
                            '.'
                        ), '', $value);
                        $value = str_replace(',', '.', $value);
                        break;
                }
            }
            return $item;
        }, $data);
    }

    /**
     * @return string
     */
    public function _getFilePath()
    {
        $file = $this->getArg('from');
        if (!$file) {
            Mage::throwException('Missing import file');
        }
        return Mage::getConfig()->getVarDir() . DS . $file;
    }

    /**
     * @param $productData
     * @param $websiteIds
     */
    public function _duplicate($productData, $websiteIds)
    {
        $groupedId = Mage::getModel('catalog/product')->getIdBySku($productData[self::GROUP_SKU_MAP_INDEX]);
        $simpleId = Mage::getModel('catalog/product')->getIdBySku($productData[self::EXISTING_SIMPLE_SKU_MAP_INDEX]);
        $newSimpleId = Mage::getModel('catalog/product')->getIdBySku($productData[self::NEW_SIMPLE_SKU_MAP_INDEX]);

        //Skip if origin product does not exists
        if (!$groupedId || !$simpleId) {
            Mage::throwException('Origin product does not exists');
        }

        //Skip if target product sku already exists
        if ($newSimpleId) {
            Mage::throwException('Simple product already exists');
        }

        /** @var Mage_Catalog_Model_Product $targetProduct */
        $targetProduct = Mage::getModel('catalog/product')->load($simpleId)->setWebsiteIds($websiteIds)->duplicate();
        $targetProduct->load($targetProduct->getId())
            ->addData(array(
                'name' => $productData[self::NEW_SIMPLE_SKU_MAP_INDEX],
                'sku' => $productData[self::NEW_SIMPLE_SKU_MAP_INDEX],
                'price' => $productData[self::PRICE_MAP_INDEX],
                'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED
            ))
            ->save();
        Mage::getModel('catalog/product_link_api')->assign('grouped', $groupedId, $targetProduct->getId());
    }
}

$class = new Oleoweb_Shell_Catalog_Import_Simples();
$class->run();
