<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Block_Category_List extends Mage_Catalog_Block_Product_List
{
    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $category = $this->getCurrentCategory();
            $this->_productCollection = $category->getCollection()
                ->addAttributeToSelect('custom_url')
                ->addAttributeToSelect('url_key')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('list_image')
                ->addFieldToFilter('parent_id', $category->getId())
                ->addFieldToFilter('is_active', true)
                ->setOrder('position', Varien_Db_Select::SQL_ASC)
                ->joinUrlRewrite();
        }

        return $this->_productCollection;
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        return Mage::registry('current_category');
    }
}
