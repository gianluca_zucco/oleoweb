<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @method setParentCategoryId(int $parentCategoryId)
 * @method int|null getParentCategoryId()
 */
class Oleoweb_Catalog_Block_Product_View_Navigation extends Mage_Core_Block_Template
{
    protected $_collection;

    /**
     * @return Mage_Catalog_Model_Resource_Category_Collection|Varien_Data_Collection
     */
    public function getCollection()
    {
        if (!isset($this->_collection)) {
            try {
                $_collection = Mage::getResourceModel('catalog/category_collection')
                    ->addFieldToFilter('parent_id', $this->getParentCategoryId())
                    ->addAttributeToFilter('is_active', true)
                    ->addAttributeToFilter('include_in_menu', true)
                    ->addAttributeToSelect('*');
                $this->_collection = $_collection;
            } catch (Exception $e) {
                return new Varien_Data_Collection();
            }
        }
        return $this->_collection;
    }
}
