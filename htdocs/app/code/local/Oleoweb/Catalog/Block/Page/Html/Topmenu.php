<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @method Varien_Data_Tree_Node getMenuTree()
 * @method string getChildrenWrapClass()
 */

class Oleoweb_Catalog_Block_Page_Html_Topmenu extends Webcomm_BootstrapNavigation_Block_Page_Html_Topmenu
{
    public function _toHtml()
    {
        return $this->_getHtml($this->getMenuTree(), $this->getChildrenWrapClass());
    }

    /**
     * {@inheritDoc}
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            if ($child->hasChildren()) {
                $outermostClassCode .= ' data-toggle="dropdown" ';
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>';
            $html .= $this->escapeHtml($child->getName());
            if ($child->hasChildren()) {
                if ($childLevel == 0) {
                    $html .= ' <b class="fa fa-caret-down"></b>';
                } else {
                    $html .= ' <b class="fa fa-caret-right"></b>';
                }
            }
            $html .= '</span></a>';

            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . ' dropdown-menu">';
                if (
                    Mage::getStoreConfig('catalog/navigation/top_in_dropdown')
                    && $childLevel == 0
                ) {
                    $prefix = Mage::getStoreConfig('catalog/navigation/top_in_dropdown_prefix');
                    $suffix = Mage::getStoreConfig('catalog/navigation/top_in_dropdown_suffix');
                    $html .= '<li class="level1 level-top-in-dropdown">';
                    $html .= '<a href="'.$child->getUrl().'"><span>';
                    $html .= $this->escapeHtml($this->__($prefix).' '.$child->getName().' '.$suffix);
                    $html .= '</span></a>';
                    $html .= '</li>';
                    $html .= '<li class="divider"></li>';
                }
                $html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            }
            $html .= '</li>';

            $counter++;
        }

        return $html;
    }
}
