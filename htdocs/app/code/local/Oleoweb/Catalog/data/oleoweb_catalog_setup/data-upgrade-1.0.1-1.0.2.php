<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

/** @var Mage_Catalog_Model_Category $_targetCategory */
$_targetCategory = Mage::getModel('catalog/category')->load(22);

/** @var Mage_Cms_Model_Block $_staticBlock */
$_staticBlock = Mage::getModel('cms/block')
    ->setTitle('Where we are')
    ->setContent('<style>
      #map {
        width: 100%;
        height: 400px;
        background-color: grey;
      }
    </style>
<div id="map"></div>
<script>
      function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById(\'map\'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    {{config path="oleoweb_catalog/config/maps_api_key"}}
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaYc_8R1cQ0DEmy29riCbcc_4Brx1Dhrc&callback=initMap">
    </script>')
    ->setIsActive(true)
    ->setIdentifier('where-we-are')
    ->setStores(array_keys(Mage::app()->getStores()));

$_staticBlock->save();

$_targetCategory->setDisplayMode(Mage_Catalog_Model_Category::DM_PAGE)->setLandingPage($_staticBlock->getId())->save();
