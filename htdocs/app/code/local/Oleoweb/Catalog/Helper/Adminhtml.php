<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Helper_Adminhtml extends Mage_Adminhtml_Helper_Data
{
    public function productListExportButtonData(): array
    {
        return [
            'label'   => $this->__('Export List'),
            'onclick' => "setLocation('{$this->getUrl('*/*/exportProductList')}')",
            'class'   => 'save'
        ];
    }
}
