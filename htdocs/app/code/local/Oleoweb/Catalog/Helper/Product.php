<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Helper_Product extends Mage_Core_Helper_Abstract
{
    public function getPageTitle()
    {
        return trim(reset(explode('|', Mage::registry('current_product')->getName())));
    }

    public function getPageSubtitle()
    {
        return trim(end(explode('|', Mage::registry('current_product')->getName())));
    }
}
