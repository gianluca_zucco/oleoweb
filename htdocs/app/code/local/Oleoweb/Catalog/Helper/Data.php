<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

use Mage_Catalog_Model_Category as Category;
use Mage_Catalog_Model_Product as Product;

class Oleoweb_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CATEGORY_CUSTOM_URL_CODE = 'custom_url';

    /**
     * Returns product attribute store label by given code
     *
     * @param $code
     * @return mixed
     */
    public function getAttributeLabel($code)
    {
        return str_replace(array('{br}'), array('<br/>'), Mage::getSingleton('eav/config')->getAttribute(Product::ENTITY, $code)->getStoreLabel());
    }

    /**
     * Return Category list image url
     *
     * @param Mage_Catalog_Model_Category $_category
     * @return bool|string
     */
    public function getCategoryListImageUrl(Category $_category)
    {
        $url = false;
        if ($image = $_category->getData('list_image')) {
            $url = Mage::getBaseUrl('media').'catalog/category/'.$image;
        }
        return $url;
    }

    /**
     * Return Category URL
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function renderCategoryUrl(Category $category)
    {
        if (!$string = $category->getData(self::CATEGORY_CUSTOM_URL_CODE)) {
            return $category->getUrl();
        }
        if (preg_match('/^{{/', $string))   {
            return Mage::helper('cms')->getBlockTemplateProcessor()->filter($string);
        }
        return $string;
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     * @return bool
     */
    public function isZeroIvaCheckout(Mage_Sales_Model_Quote_Address $address)
    {
        $billingData = $this->_getRequest()->getParam('billing', array());
        return (
            (isset($billingData['business_account']) && in_array($address->getCountryId(), Mage::getStoreConfig('customer/address/eu_countries'))) ||
            (!in_array($address->getCountryId(), Mage::getStoreConfig('customer/address/eu_countries')))
        ) && $address->getCountryId() != 'IT';
    }
}
