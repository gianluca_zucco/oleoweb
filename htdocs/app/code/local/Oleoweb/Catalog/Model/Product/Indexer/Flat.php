<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Model_Product_Indexer_Flat extends Mage_Catalog_Model_Product_Indexer_Flat
{
    public function reindexAll()
    {
        if(Mage::helper('catalog/product_flat')->isEnabled()) {
            parent::reindexAll();
        }
    }
}
