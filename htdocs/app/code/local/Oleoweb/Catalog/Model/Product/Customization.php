<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Model_Product_Customization extends Varien_Object
{
    const CUSTOMIZATION_NONE = 'none';
    const CUSTOMIZATION_CUSTOM = 'custom';
}
