<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Model_Product_Meta extends Varien_Object implements Oleoweb_Catalog_Model_Interface_Meta
{
    /**
     * @return string
     */
    public function getMetaDescription()
    {
        $product = $this->getEntity();
        if ($product->getMetaDescription()) {
            return $product->getMetaDescription();
        }
        return Mage::helper('oleoweb_catalog')->__('Looking for %s?  On Oleoweb Shop you can order and receive this product within few days. Buy now ...', $product->getName());
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        $product = $this->getEntity();
        return $product->getMetaTitle() ?? "{$product->getName()} - Online Sale";
    }

    /**
     * @param Mage_Catalog_Model_Product|Mage_Core_Model_Abstract $entity
     * @return $this
     */
    public function setEntity(Mage_Core_Model_Abstract $entity)
    {
        $this->setData('entity', $entity);
        return $this;
    }

    /**
     * @return Mage_Catalog_Model_Product|Mage_Core_Model_Abstract
     */
    public function getEntity()
    {
        return $this->getData('entity');
    }
}
