<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Model_System_Config_Backend_Csv extends Mage_Core_Model_Config_Data
{
    protected function _afterLoad()
    {
        $value = $this->getValue();
        $this->setValue(array_map('trim', explode(',', $value)));
    }

    protected function _beforeSave()
    {
        if (is_array($this->getValue())) {
            $this->setValue(implode(',', $this->getValue()));
        }
    }
}
