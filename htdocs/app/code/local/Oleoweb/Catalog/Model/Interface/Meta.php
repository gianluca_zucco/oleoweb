<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

interface Oleoweb_Catalog_Model_Interface_Meta
{
    /**
     * @return string
     */
    public function getMetaTitle();

    /**
     * @return string
     */
    public function getMetaDescription();

    /**
     * @param Mage_Core_Model_Abstract $entity
     * @return $this
     */
    public function setEntity(Mage_Core_Model_Abstract $entity);

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function getEntity();
}
