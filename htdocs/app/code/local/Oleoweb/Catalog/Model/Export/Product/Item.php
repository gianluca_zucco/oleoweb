<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Model_Export_Product_Item
{
    /**
     * @param $product
     * @return Closure
     */
    public static function getData(Mage_Catalog_Model_Product $product): Closure
    {
        return function ($key) use ($product) {
            $stockItem = $product->getStockItem() ?? new Varien_Object();
            return $product->getData($key) ?? $stockItem->getData($key);
        };
    }
}
