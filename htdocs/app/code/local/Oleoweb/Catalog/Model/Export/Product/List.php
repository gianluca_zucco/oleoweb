<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */
use Oleoweb_Catalog_Model_Export_Product_Item as Item;

class Oleoweb_Catalog_Model_Export_Product_List
{
    private $allowed;
    private $collection;

    public function __construct()
    {
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*');
        Mage::getSingleton('cataloginventory/stock')->addItemsToProducts($collection);
        $this->collection = $collection;

        $config = Mage::getStoreConfig('oleoweb_catalog/export/allowed_attributes');
        $this->allowed = array_map('trim', explode(',', $config));
    }


    /**
     * @return string[]
     * @throws Exception
     */
    public function getCsvFile(): array
    {
        $io = new Varien_Io_File();
        $file = $this->initStream($io);

        foreach ($this->getItems() as $item) {
            $io->streamWriteCsv($item);
        }

        $io->streamUnlock();
        $io->streamClose();

        return [
            'type'  => 'filename',
            'value' => $file,
            'rm'    => true
        ];
    }

    private function getItems(): \Generator
    {
        yield $this->allowed;
        foreach ($this->collection as $product) {
            yield array_map(Item::getData($product), $this->allowed);
        }
    }

    /**
     * @param Varien_Io_File $io
     * @return string
     * @throws Exception
     */
    private function initStream(Varien_Io_File $io): string
    {
        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . DS . $name . '.csv';

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);
        return $file;
    }
}
