<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

use Varien_Event_Observer as Event;
use Oleoweb_Catalog_Model_Product_Customization as Customization;
use Mage_Catalog_Model_Product as Product;
use Oleoweb_Catalog_Helper_Data as CatalogHelper;
use Mage_Catalog_Model_Category as Category;

class Oleoweb_Catalog_Model_Observer
{

    const CUSTOM_URL_CODE = 'custom_url';

    public function setCanShowPrice(Event $observer)
    {
        $product = $observer->getProduct();
        $product->setCanShowPrice(Mage::getStoreConfigFlag('oleoweb_catalog/config/show_product_price'));
    }

    /**
     * Replace default url with custom link if requested
     * @todo improve in recursive style
     * @param Varien_Event_Observer $observer
     */
    public function addCustomLink(Event $observer)
    {
        /** @var Varien_Data_Tree_Node $menu */
        $menu = $observer->getMenu();

        $_values = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToFilter(CatalogHelper::CATEGORY_CUSTOM_URL_CODE, array('notnull' => true))
            ->addAttributeToFilter(CatalogHelper::CATEGORY_CUSTOM_URL_CODE, array('neq' => ''));

        if ($_values->count()) {
            /** @var Varien_Data_Tree_Node $child */
            foreach ($menu->getChildren() as $key => $child)    {
                $_category = $_values->getItemById(str_replace('category-node-', '', $key));
                if ($_category instanceof Category) {
                    if ($_category->getData(CatalogHelper::CATEGORY_CUSTOM_URL_CODE)) {
                        $child->setUrl(Mage::helper('oleoweb_catalog')->renderCategoryUrl($_category));
                    }
                }
                if ($child->hasChildren())  {
                    foreach ($child->getChildren() as $key2 => $child2)  {
                        $_category2 = $_values->getItemById(str_replace('category-node-', '', $key2));
                        if ($_category2 instanceof Category) {
                            if ($_category2->getData(CatalogHelper::CATEGORY_CUSTOM_URL_CODE)) {
                                $child2->setUrl(Mage::helper('oleoweb_catalog')->renderCategoryUrl($_category2));
                            }
                        }
                        if ($child2->hasChildren())  {
                            foreach ($child->getChildren() as $key3 => $child3)  {
                                $_category3 = $_values->getItemById(str_replace('category-node-', '', $key3));
                                if ($_category3 instanceof Category) {
                                    if ($_category3->getData(CatalogHelper::CATEGORY_CUSTOM_URL_CODE)) {
                                        $child3->setUrl(Mage::helper('oleoweb_catalog')->renderCategoryUrl($_category3));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function setCategoryMetaData(Event $observer)
    {
        /** @var Category $category */
        $category = $observer->getCategory();
        $model = Mage::getModel('oleoweb_catalog/category_meta')->setEntity($category);
        $category->setMetaTitle($model->getMetaTitle());
        $category->setMetaDescription($model->getMetaDescription());
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function setProductMetaData(Event $observer)
    {
        /** @var Product $product */
        $product = $observer->getProduct();
        $model = Mage::getModel('oleoweb_catalog/product_meta')->setEntity($product);
        $product->setMetaTitle($model->getMetaTitle());
        $product->setMetaDescription($model->getMetaDescription());
    }

    /**
     * @listen to sales_quote_add_item
     * @param Varien_Event_Observer $observer
     */
    public function addCustomOption(Event $observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = $observer->getQuoteItem();
        $request = Mage::app()->getRequest();
        $custom = $request->getParam('customization', Customization::CUSTOMIZATION_NONE);
        if ($custom == Customization::CUSTOMIZATION_CUSTOM)   {
            $item->addOption(
                array(
                    'code'  => 'additional_options',
                    'value' => serialize($additionalOptions = array(
                        array(
                            'code'  => 'customization',
                            'label' => Mage::helper('oleoweb_catalog')->__('Marking'),
                            'value' => Mage::helper('oleoweb_catalog')->__('Customized marking')
                        )
                    )),
                )
            );
            $item->getProduct()->setIsSuperMode(true);
            $currentPrice = (float) $item->getProduct()->getPrice();
            $fee = round($currentPrice * 10/100, 1);
            $item->getProduct()->setIsSuperMode(true);
            $item->setCustomPrice($currentPrice + $fee);
            $item->setOriginalCustomPrice($currentPrice + $fee);
        } else {
            $item->addOption(
                array(
                    'code'  => 'additional_options',
                    'value' => serialize($additionalOptions = array(
                        array(
                            'code'  => 'customization',
                            'label' => Mage::helper('oleoweb_catalog')->__('Marking'),
                            'value' => Mage::helper('oleoweb_catalog')->__('Standard Oleoweb marking')
                        )
                    )),
                )
            );
        }
    }

    /**
     * @listen to sales_convert_quote_item_to_order_item
     * @param Varien_Event_Observer $observer
     */
    public function copyOptionToOrderItem(Event $observer)
    {
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $orderItem = $observer->getOrderItem();
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
    }

    /**
     * Listen to
     * - sales_quote_address_save_before
     * @param Varien_Event_Observer $observer
     */
    public function toggleBusinessAccount(Event $observer)
    {
        /** @var Mage_Sales_Model_Quote_Address $address */
        $address = $observer->getQuoteAddress();
        if ($address->getAddressType() == Mage_Sales_Model_Quote_Address::TYPE_BILLING)    {
            if(Mage::app()->getRequest()->getActionName() == 'saveBilling') {
                $billingData = Mage::app()->getRequest()->getParam('billing', array());
                $address->setData('business_account', isset($billingData['business_account']));
            }
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function clearProductCache(Event $observer)
    {
        $helper = Mage::helper('oleoweb_catalog');
        try {
            Mage::app()->getCacheInstance()->cleanType(Mage_Core_Block_Abstract::CACHE_GROUP);
            Mage::getSingleton('adminhtml/session')->addSuccess($helper->__("Block cache cleaned"));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addSuccess($helper->__("Error on block cache cleaning: %s", $e->getMessage()));
        }
    }
}
