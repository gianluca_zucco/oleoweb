<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Model_Category_Meta extends Varien_Object implements Oleoweb_Catalog_Model_Interface_Meta
{
    /**
     * @return string
     */
    public function getMetaDescription()
    {
        $category = $this->getEntity();
        if ($category->getMetaDescription()) {
            return $category->getMetaDescription();
        }
        return Mage::helper('oleoweb_catalog')->__('Looking for %s? On Oleoweb Shop you can order and receive this product within few days. Buy now ...', $category->getName());
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        $category = $this->getEntity();
        if ($category->getMetaTitle()) {
            return $category->getMetaTitle();
        }
        switch ($category->getLevel())  {
            case 2:
                return "{$category->getName()} - Online Sale";
            break;
            case 3:
            case 4:
                return "{$category->getName()} - {$category->getParentCategory()->getName()} - Online Sale";
            break;
        }
        return '';
    }

    /**
     * @param Mage_Catalog_Model_Category|Mage_Core_Model_Abstract $entity
     * @return $this
     */
    public function setEntity(Mage_Core_Model_Abstract $entity)
    {
        $this->setData('entity', $entity);
        return $this;
    }

    /**
     * @return Mage_Catalog_Model_Category|Mage_Core_Model_Abstract
     */
    public function getEntity()
    {
        return $this->getData('entity');
    }
}
