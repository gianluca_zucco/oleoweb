<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Oleoweb_Catalog_Model_Category_Indexer_Flat extends Mage_Catalog_Model_Category_Indexer_Flat
{
    public function reindexAll()
    {
        if(Mage::helper('catalog/category_flat')->isEnabled()) {
            parent::reindexAll();
        }
    }
}
