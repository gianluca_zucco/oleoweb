<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . '/Catalog/ProductController.php';

class Oleoweb_Catalog_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    public function exportProductListAction()
    {
        try {
            $arguments = [
                'productExport.csv',
                Mage::getModel('oleoweb_catalog/export_product_list')->getCsvFile(),
                'text/csv'
            ];
            $this->_prepareDownloadResponse(...$arguments);
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $e->getMessage());
            $this->_redirect('*/*');
        }
    }
}
