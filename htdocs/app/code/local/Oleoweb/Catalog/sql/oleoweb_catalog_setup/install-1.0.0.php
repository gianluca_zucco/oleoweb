<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'webrotate_path', array(
    'type' => 'text',
    'input' => 'text',
    'required' => false,
    'group' =>  'General',
    'is_configurable' => false,
    'used_in_product_listing' => false
));
$installer->endSetup();
