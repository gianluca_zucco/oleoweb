<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'custom_url', array(
    'label' => 'Custom url',
    'required' => false,
    'group' =>  'General Information',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
));

$installer->endSetup();
