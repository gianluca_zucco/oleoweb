<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'net_weight', array(
    'label' => 'Net Weight',
    'type' => 'text',
    'input' => 'text',
    'required' => false,
    'group' =>  'Oleoweb',
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'apply_to' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'dimension', array(
    'label' => 'Dimension',
    'type' => 'text',
    'input' => 'text',
    'required' => false,
    'group' =>  'Oleoweb',
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'apply_to' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'max_flow_capacity', array(
    'label' => 'Max Flow Capacity',
    'type' => 'text',
    'input' => 'text',
    'required' => false,
    'group' =>  'Oleoweb',
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'apply_to' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'max_pressure', array(
    'label' => 'Max Pressure',
    'type' => 'text',
    'input' => 'text',
    'required' => false,
    'group' =>  'Oleoweb',
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'apply_to' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
));

$installer->endSetup();
