<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'list_image', array(
    'label' => 'List image',
    'type' => 'varchar',
    'input' => 'image',
    'backend' => 'catalog/category_attribute_backend_image',
    'required' => false,
    'group' =>  'General Information',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
));

$installer->endSetup();
