<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'product_code', array(
    'label' => 'Product Code',
    'type' => 'text',
    'input' => 'text',
    'required' => true,
    'group' =>  'Oleoweb',
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'apply_to' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
));

$installer->endSetup();
