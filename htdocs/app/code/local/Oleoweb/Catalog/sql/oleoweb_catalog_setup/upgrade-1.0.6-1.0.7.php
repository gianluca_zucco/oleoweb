<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->startSetup();
$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'qty_min');
$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'qty_max');
$installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'qty_increment');
$installer->endSetup();
