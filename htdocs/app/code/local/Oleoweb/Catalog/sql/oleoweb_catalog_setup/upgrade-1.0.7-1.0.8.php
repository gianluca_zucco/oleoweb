<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 *
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;

$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'is_custom_mark_enabled', array(
    'label' => 'Custom mark enabled',
    'type' => 'int',
    'input' => 'boolean',
    'required' => false,
    'group' =>  'Oleoweb',
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'apply_to' => Mage_Catalog_Model_Product_Type::TYPE_GROUPED
));
$installer->endSetup();
