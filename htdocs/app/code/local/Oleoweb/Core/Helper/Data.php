<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

use Mage_Core_Model_Store as Store;

class Oleoweb_Core_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param Mage_Core_Model_Store $store
     * @return null|string
     */
    public function getCmsAlternateLink(Store $store)
    {
        $page = Mage::getSingleton('cms/page');
        $seoId = $page->getData('seo_identifier');
        if (!$seoId) {
            return $store->getUrl('/');
        }

        $otherLanguagePage = Mage::getResourceModel('cms/page_collection')
            ->addFieldToFilter('seo_identifier', $seoId)
            ->addStoreFilter($store, false)
            ->getFirstItem();

        return !$otherLanguagePage->getId() ? $store->getUrl('/') : $store->getUrl('/', array(
            '_direct' => $otherLanguagePage->getIdentifier()
        ));
    }

    /**
     * @param Mage_Core_Model_Store $store
     * @return string
     */
    public function getCategoryAlternateUrl(Store $store)
    {
        $category = Mage::registry('current_category');
        $url = Mage::getModel('core/url_rewrite')->setStoreId($store->getId())->loadByIdPath('category/' . $category->getId());
        return $store->getUrl('/', array('_direct' => $url->getRequestPath()));
    }

    /**
     * @param Mage_Core_Model_Store $store
     * @return string
     */
    public function getProductAlternateUrl(Store $store)
    {
        $product = Mage::registry('current_product');
        return $product->setStoreId($store->getId())->getUrlInStore();
    }
}
