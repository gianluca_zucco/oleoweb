<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project oleoweb
 */

class Zeta_Zcache_Helper_Data extends Mage_Core_Helper_Abstract 
{
    /**
     * @return string
     */
    public function getCategoryViewCacheKey()
    {
        return $this->_getCacheKey('catalog/category_view', array(Mage::registry('current_category')->getId()));
    }

    /**
     * @param string $baseBlock
     * @return array
     */
    protected function _getBaseCacheKeyInfo($baseBlock)
    {
        return Mage::getBlockSingleton($baseBlock)->getCacheKeyInfo();
    }

    /**
     * @param array $additional
     * @return string
     */
    protected function _getCacheKey($baseBlock, array $additional = array())
    {
        return md5(implode('|', array_merge($this->_getBaseCacheKeyInfo($baseBlock), $additional)));
    }
}
