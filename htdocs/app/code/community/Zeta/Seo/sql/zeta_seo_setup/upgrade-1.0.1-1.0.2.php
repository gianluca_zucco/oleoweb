<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 *
 * @var $this Mage_Core_Model_Resource_Setup
 */

$installer = $this;

$installer->getConnection()->addColumn($this->getTable('cms/page'), 'seo_identifier', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => Varien_Db_Ddl_Table::DEFAULT_TEXT_SIZE,
    'nullable' => true,
    'comment' => 'Connect pages trough store views'
));


