<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 *
 * @var $this Mage_Core_Model_Resource_Setup
 */

$installer = $this;

$tableName = $this->getTable('zeta_seo/redirect');

$table = $installer->getConnection()->newTable($tableName)
    ->addColumn('redirect_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'primary' => true,
        'comment' => 'Row ID'
    ))
    ->addColumn('redirect_from', Varien_Db_Ddl_Table::TYPE_TEXT, Varien_Db_Ddl_Table::DEFAULT_TEXT_SIZE, array(
        'nullable' => false,
        'comment' => 'Redirect from'
    ))
    ->addColumn('redirect_to', Varien_Db_Ddl_Table::TYPE_TEXT, Varien_Db_Ddl_Table::DEFAULT_TEXT_SIZE, array(
        'nullable' => false,
        'comment' => 'Redirect to'
    ))
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'default' => '301'
    ))->setComment('Redirect table');

$installer->getConnection()->createTable($table);
