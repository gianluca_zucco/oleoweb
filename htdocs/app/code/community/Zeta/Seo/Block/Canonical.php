<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Block_Canonical extends Zeta_Seo_Block_Abstract
{
    /**
     * @return mixed
     */
    public function getCanonical()
    {
        $_request = $this->getRequest();
        return Mage::helper('zeta_seo')->getCanonicalStore()->getUrl($_request->getRequestUri(), array(
            '_current' => true
        ));
    }

    protected function _toHtml()
    {
        return Mage::helper('zeta_seo')->isCanonicalStore() ? '' : parent::_toHtml();
    }
}
