<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project arya-italian-jewels
 */

class Zeta_Seo_Block_Cms_Meta extends Zeta_Seo_Block_Abstract
{
    public function getAlternateLink(Mage_Core_Model_Store $store)
    {
        $page = Mage::getSingleton('cms/page');
        $seoId = $page->getData('seo_identifier');
        if (!$seoId) {
            return null;
        }

        $otherLanguagePage = Mage::getResourceModel('cms/page_collection')
            ->addFieldToFilter('seo_identifier', $seoId)
            ->addStoreFilter($store, false)
            ->getFirstItem();

        if (!$otherLanguagePage->getId()) return null;

        return $store->getUrl('/', array(
            '_direct' => $otherLanguagePage->getIdentifier()
        ));
    }
}
