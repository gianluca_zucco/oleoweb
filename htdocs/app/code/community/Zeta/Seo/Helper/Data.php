<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Zeta_Seo_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CATEGORY_TAB_SEO = 'SEO';
    const TYPE_PRODUCT = 'product';
    const SCHEMA_URL = 'http://www.schema.org';

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag('zeta_seo/settings/is_active');
    }

    /**
     * Returns snippet information
     *
     * @param string $_type
     * @return array
     */
    public function getCommonSnippetInformation($_type = self::TYPE_PRODUCT)
    {
        $_data = array(
            "@context" => self::SCHEMA_URL,
            "@type" => $_type
        );

        if ($_type == self::TYPE_PRODUCT) {
            $_data['brand'] = array(
                '@type' => 'Thing',
                'name' => $this->getBrand()
            );
        }
        return $_data;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return Mage::getStoreConfig('zeta_seo/settings/brand');
    }

    /**
     * @return string
     */
    public function getCmsHomepageTitle()
    {
        return Mage::getStoreConfig('design/head/default_title');
    }
}
