<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getConfiguredAvailableElements()
    {
        return Mage::getStoreConfig('mzentrale_widget/available_row_elements');
    }

    public function getConfiguredSliderElements()
    {
        return Mage::getStoreConfig('mzentrale_widget/available_slider_elements');
    }

    public function getConfiguredPageAvailableElements()
    {
        return Mage::getStoreConfig('mzentrale_widget/available_page_elements');
    }

    public function getWysiwygUrl($targetFieldName)
    {
        return $this->_getUrl('*/cms_wysiwyg_images/index/', array(
            'target_element_id' => '#{field_prefix}_' . $targetFieldName
        ));
    }

    /**
     * @param string $hex
     * @param string $trasparency
     * @return string
     */
    public function toRgba($hex, $trasparency = '.6') {
        $hex = ltrim($hex, '#');
        $rgba = str_split($hex, 2);
        array_walk($rgba, array($this, '_hexdec'));
        array_push($rgba, $trasparency);
        return implode(',', $rgba);
    }

    /**
     * Returns today highlight product
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getTodayHighlightCollection()
    {
        $today = new Zend_Date();
        $today = $today->toString('Y-M-d');
        return Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addAttributeToFilter('special_price', array('notnull' => true))
            ->addAttributeToFilter('today_highlight', array('lteq' => $today))
            ->addAttributeToFilter('today_highlight', array('notnull' => true))
            ->setOrder('today_highlight', 'DESC')
            ->setPage(0,1);
    }

    protected function _hexdec(&$item, $key)    {
        $item = hexdec($item);
    }
}
