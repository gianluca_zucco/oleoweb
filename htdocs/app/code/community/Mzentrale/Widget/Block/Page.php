<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Page extends Mzentrale_Widget_Block_Abstract
{
    protected $_template = 'mzentrale/widget/row.phtml';
}
