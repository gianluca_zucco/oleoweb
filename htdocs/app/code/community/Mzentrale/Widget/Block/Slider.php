<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 */

class Mzentrale_Widget_Block_Slider extends Mzentrale_Widget_Block_Abstract implements Mage_Widget_Block_Interface
{
    protected $_template = 'mzentrale/widget/slider.phtml';
}
