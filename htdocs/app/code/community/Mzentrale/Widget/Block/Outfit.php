<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Outfit extends Mzentrale_Widget_Block_Abstract
{
    protected $_template = 'mzentrale/widget/outfit.phtml';

    /**
     * @return array|Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProducts()
    {
        if (!$this->getData('products')) {
            return array();
        }

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addCategoryIds()
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes())
            ->addFieldToFilter('sku', array('in' => explode(',', $this->getData('products'))))
            ->setPage(0, Mage::getStoreConfig('mzentrale_widget/outfit/maximum_allowed_products'));

        $collection->load();

        //Add category names
        $availableCategories = Mage::getResourceModel('catalog/category_collection')
            ->addNameToResult()
            ->addFieldToFilter('parent_id', Mage::getStoreConfig('mzentrale_widget/outfit/product_root_category'))
            ->getItems();

        $this->setAvailableCategories($availableCategories);
        return $collection->walk(array($this, 'addCategoryNameToProduct'));
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product
     */
    public function addCategoryNameToProduct($product)
    {
        $categories = $this->getAvailableCategories();
        $categoryId = array_intersect($product->getCategoryIds(), array_keys($categories));
        if ($categoryId)  {
            $category = $categories[reset($categoryId)];
            $product->setCategoryName($category->getName());
        }
        return $product;
    }
}
