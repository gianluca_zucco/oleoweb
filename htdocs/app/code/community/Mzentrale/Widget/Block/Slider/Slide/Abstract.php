<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 */

abstract class Mzentrale_Widget_Block_Slider_Slide_Abstract extends Mzentrale_Widget_Block_Abstract
{
    protected $_template = 'mzentrale/widget/slider/slide/default.phtml';

    public function getTitle()
    {
        return ucfirst($this->getData('title'));
    }
}
