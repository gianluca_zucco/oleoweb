<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Slider_Slide_Action extends Mzentrale_Widget_Block_Slider_Slide_Abstract
{
    protected $_template = 'mzentrale/widget/slider/slide/action.phtml';

    public function getTextAlignment($fieldId = 'text_alignment')
    {
        return Mzentrale_Widget_Model_Source_Alignment::ALIGN_CENTER;
    }

    /**
     * @param $fieldId
     * @return string
     */
    protected function _getRgba($fieldId, $trasparency = '1')
    {
        return parent::_getRgba($fieldId,$trasparency);
    }

    public function getContentContainerCssClass()
    {
        return sprintf('background-color: %s; color: %s;',
            $this->getLayerBackgroundColor(),
            $this->getTextColor()
        );
    }
}
