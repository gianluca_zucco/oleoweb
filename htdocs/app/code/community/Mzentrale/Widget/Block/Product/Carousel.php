<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 * @method string getProducts()
 */

class Mzentrale_Widget_Block_Product_Carousel extends Mzentrale_Widget_Block_Product
{
    protected $_template = 'mzentrale/widget/product/carousel.phtml';
    protected $_productCollection = array();
    protected $_defaultQtyPerBreakPoints = array(
        'lg' => 12,
        'md' => 6,
        'sm' => 2,
        'xs' => 1
    );

    /**
     * @return array|Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection()
    {
        if (!$this->_productCollection) {
            $productSkus = $this->getProducts();

            if ($productSkus)   {

                $productSkus = explode(',', $productSkus);

                /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
                $collection = Mage::getResourceModel('catalog/product_collection')
                    ->addUrlRewrite('')
                    ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes())
                    ->addFieldToFilter('sku', array('in' => $productSkus));
                $this->_productCollection = $collection->load();;
            } else {
                $_fallbackCollection = $this->_getFallbackCollection();
                if ($_fallbackCollection)   {
                    $this->_productCollection = $_fallbackCollection->load();
                }
            }
            //Eventually add limit
            if ($this->getLimit() && is_numeric($this->getLimit()))  {
                $this->_productCollection->setPage(0, $this->getLimit());
            }

            if ($this->getIsRandom())   {
                $this->_productCollection->getSelect()->order(new Zend_Db_Expr('RAND()'));
            }

        }
        return $this->_productCollection;
    }

    /**
     * @param array $productSkus
     * @return int
     */
    public function getCount(array $productSkus = array())
    {
        $maximum = $this->getMaximumCount();
        return $maximum - count($productSkus);
    }



    /**
     * @return Mage_Catalog_Model_Category
     */
    public function getFallBackCategory()
    {
        preg_match('/category\/([\d]+)/', $this->getData('category'), $matches);
        $id = end($matches);
        return Mage::getModel('catalog/category')->load($id);
    }

    /**
     * @return mixed
     */
    public function getMaximumCount()
    {
        $maximum = Mage::getStoreConfig('mzentrale_widget/product_teaser/count');
        return $maximum;
    }

    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _getFallbackCollection()
    {
        $fallBackCategory = $this->getFallBackCategory();
        if ($fallBackCategory->getId()) {
            /** @var Mage_Catalog_Model_Resource_Product_Collection $_fallbackCollection */
            $_fallbackCollection = $fallBackCategory->getProductCollection();
            $_fallbackCollection
                ->addUrlRewrite($fallBackCategory->getId())
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductCollectionAttributes())
                ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->addOrder('news_to_date', 'DESC');

            $_fallbackCollection->load();
            return $_fallbackCollection;
        }
    }

    public function getTextAlignment($fieldId = 'text_alignment')
    {
        return Mzentrale_Widget_Model_Source_Alignment::ALIGN_CENTER;
    }

    public function getProductColumnCssClass()
    {
        $count = max(count($this->getProductCollection()), 1);
        return sprintf('col-xs-%s', 12/$count);
    }

    public function getInterval()
    {
        return ($this->getData('interval') && is_numeric($this->getData('interval'))) ? $this->getData('interval') : 'false';
    }

    /**
     * @return string
     */
    public function getGridClass()
    {
        return sprintf('col-sm-%s', $this->getGrid());
    }

    /**
     * @return string
     */
    public function getItemGridClass()
    {
        return sprintf('col-sm-%s', intval(12 / $this->getItemBlockQty()));
    }

    /**
     * Returns bootstrap codified breakpoints
     *
     * @return array
     */
    public function getBreakPoints()
    {
        return array('lg', 'md', 'sm', 'xs');
    }

    /**
     * @param null $breakpoint
     * @return array|int
     */
    public function getQtyPerBreakPoint($breakpoint = null)
    {
        $values = array();
        foreach ($this->getBreakPoints() as $_breakpoint)    {
            $_config = $this->getData('item_block_qty_' . $_breakpoint);
            $values[$_breakpoint] = !$_config ? $this->_defaultQtyPerBreakPoints[$_breakpoint] : $_config;
        }
        if ($breakpoint && in_array($breakpoint, $this->getBreakPoints()))    {
            return $values[$breakpoint];
        }
        return $values;
    }

    public function showCarouselHeader()
    {
        return $this->getTitle();
    }

    public function showCarouselControl($breakpoint)
    {
        return $this->getProductCollection()->count() > $this->getQtyPerBreakPoint($breakpoint);
    }
}

