<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Teaser extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element implements Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Page_Element_Interface
{
    public function prepareFields()
    {
        $this->_addImageField('image'  . $this->getCode(), 'Image');
        $this->_addImageField('mobile_image'  . $this->getCode(), 'Mobile Image #1');
        $this->_addField('title'  . $this->getCode(), 'text', 'Title');
        $this->_addTextArea('text'  . $this->getCode(), 'Text');
        $this->_addField('text_color'  . $this->getCode(), 'text', 'Text color');
        $this->_addField('layer_color'  . $this->getCode(), 'text', 'Background color', array('required' => false));
        $this->_addField('transparent_layer_background'  . $this->getCode(), 'select', 'Transparent background', array(
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $this->_addField('text_alignment'  . $this->getCode(), 'select', 'Text alignment', array(
            'values' => Mage::getSingleton('mzentrale_widget/source_alignment')->toOptionArray()
        ));
        $this->_addField('button_text'  . $this->getCode(), 'text', 'Button text');
        $this->_addButtonLink('button_link'  . $this->getCode(), 'Button link');
    }
}
