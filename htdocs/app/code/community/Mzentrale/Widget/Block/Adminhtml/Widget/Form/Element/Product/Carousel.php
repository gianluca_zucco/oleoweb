<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Product_Carousel extends Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->_addField('title', 'text', 'Title');
        $this->_addYesNoSelect('is_highlight', 'Is highlighted');
        $this->_addYesNoSelect('is_random', 'Use random');
        $this->_addField('limit', 'text', 'Limit collection to', array(
            'required' => false
        ));
        $this->_addField('interval', 'text', 'Pause between slides');
        $this->_addField('item_block_qty_lg', 'text', 'Item per slider', array(
            'after_element_html' => '<pre>'.$this->__('Desktop').'</pre>',
            'class' => 'validate-digits'
        ));
        $this->_addField('item_block_qty_md', 'text', 'Item per slider', array(
            'after_element_html' => '<pre>'.$this->__('Laptop').'</pre>',
            'class' => 'validate-digits'
        ));
        $this->_addField('item_block_qty_sm', 'text', 'Item per slider', array(
            'after_element_html' => '<pre>'.$this->__('Tablet').'</pre>',
            'class' => 'validate-digits'
        ));
        $this->_addField('item_block_qty_xs', 'text', 'Item per slider', array(
            'after_element_html' => '<pre>'.$this->__('Phone').'</pre>',
            'class' => 'validate-digits'
        ));
        $this->_addProductPicker('products', 'Pick products', array('required' => false));
        $this->_addCategoryPicker('category', 'Pick category');
        return $this;
    }
}
