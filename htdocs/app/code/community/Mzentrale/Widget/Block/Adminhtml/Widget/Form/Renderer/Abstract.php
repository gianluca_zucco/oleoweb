<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

abstract class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Renderer_Abstract extends Mage_Adminhtml_Block_Template implements Varien_Data_Form_Element_Renderer_Interface
{
    protected $_template = 'mzentrale/widget/form/renderer.phtml';
    protected $_blockClass = 'mzentrale_widget/adminhtml_widget_form_element';

    abstract public function render(Varien_Data_Form_Element_Abstract $element);

    /**
     * Getter
     *
     * @return Mage_Widget_Model_Widget_Instance
     */
    public function getWidgetInstance()
    {
        return Mage::registry('current_widget_instance');
    }

    /**
     * @return array
     */
    public function getParameterByType()
    {
        $params = $this->getWidgetInstance()->getWidgetParameters();
        if ($params)    {
            foreach ($params as $param) {
                if (is_array($param)) {
                    return $param;
                }
            }
        }
        return array();
    }


    /**
     * Returns JS-ready params
     *
     * @return string
     */
    public function getParameterByTypeJson()
    {
        return $this->jsQuoteEscape(Mage::helper('core')->jsonEncode($this->getParameterByType()));
    }

    abstract public function getAvailableElementsTemplate();
}
