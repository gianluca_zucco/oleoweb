<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 * @method Varien_Data_Form getForm()
 * @method Varien_Data_Form_Element_Fieldset getFieldset()
 * @method getConfiguredChildren()
 * @method setConfiguredChildren()
 */
class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element extends Mage_Adminhtml_Block_Template
{
    protected $_formButtonsBlock = 'mzentrale_widget/adminhtml_widget_form_element_buttons';
    protected $_formButtonsTemplate = 'mzentrale/widget/form/element/buttons.phtml';
    protected $_template = 'mzentrale/widget/form/element.phtml';

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->_prepareForm();
        return parent::_beforeToHtml();
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setUseContainer(false);
        $fieldset = $form->addFieldset('#{form-id}', array(
            'legend' => '#{legend}',
            'class' => 'collapseable collapsed',
            'header_bar' => $this->getFormButtonsBlock()->toHtml()
        ));
        $fieldset->setRenderer(Mage::getSingleton('core/layout')->createBlock('mzentrale_widget/adminhtml_widget_form_renderer_fieldset'));
        $this->setForm($form);
        $this->setFieldset($fieldset);
        $this->_addColumnSelect('columns', 'Layout');
    }

    /**
     * @param $name
     * @param $type
     * @param $label
     * @param array $optional
     * @param null $renderer
     * @return Varien_Data_Form_Element_Abstract
     */
    protected function _addField($name, $type, $label, $optional = array(), $renderer = null)
    {
        $config = array_merge(array(
            'label' => $this->__($label),
            'value' => '#{' . $name . '}',
            'name' => $this->_generateFieldName($name),
            'class' => 'mzentrale_widget',
            'required' => true,
            'after' => true
        ), $optional);
        $element = $this->getFieldset()->addField('#{field_prefix}_' . $name, $type, $config);

        if ($renderer) {
            $element->setRenderer($renderer);
        }
        return $element;
    }

    /**
     * @param $name
     * @return string
     */
    protected function _generateFieldName($name)
    {
        $form = $this->getForm();
        return $form->addSuffixToName($form->addSuffixToName($form->addSuffixToName($name, '#{field_prefix}'),
            Mzentrale_Widget_Model_Instance::WIDGET_CUSTOM_FIELDNAME), 'parameters');
    }

    /**
     * @param $id
     * @param $label
     * @param array $additional
     */
    protected function _addDateField($id, $label, $additional = array())
    {
        $params = array_merge(array(
            'after_element_html' => Mage::getSingleton('core/layout')
                ->createBlock('core/template')
                ->setTemplate('mzentrale/widget/form/renderer/date.phtml')
                ->setElementName($id)
                ->toHtml()
        ), $additional);
        $this->_addField($id, 'text', $label, $params);
    }

    protected function _addTextArea($id, $label, $additional = array())
    {
        $params = array_merge(array('class' => 'validate-length maximum-length-' . Mage::getStoreConfig('mzentrale_widget/settings/text_max_character')),
            $additional);
        $this->_addField($id, 'textarea', $label, $params);
    }

    protected function _addButtonLink($id, $label, $parameters = array())
    {
        $params = array_merge(array(
            'class' => 'hidden',
            'after_element_html' => Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setFieldId($id)
                ->setTemplate('mzentrale/widget/form/renderer/link/additional.phtml')->toHtml()
        ), $parameters);
        $this->_addField($id . 'value', 'text', $label, $params);
        $this->_addField($id . 'transport', 'hidden', '', array('required' => false));
    }

    protected function _addFullOptionLink($id, $label, $parameters = array())
    {
        $params = array_merge(array(
            'after_element_html' => Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setFieldId($id)
                ->setTemplate('mzentrale/widget/form/renderer/link/fulloption.phtml')->toHtml()
        ), $parameters);
        $this->_addButtonLink($id, $label, $params);
    }

    protected function _addCategoryPicker($id, $label, $parameters = array())
    {
        $params = array_merge(array(
            'after_element_html' => Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setFieldId($id)
                ->setTemplate('mzentrale/widget/form/renderer/link/category.phtml')->toHtml()
        ), $parameters);
        $this->_addButtonLink($id, $label, $params);
    }

    protected function _addImageField($id, $label, $parameters = array())
    {
        $params = array_merge(array(
            'after_element_html' => Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setTemplate('mzentrale/widget/form/renderer/image.phtml')
                ->setElementId($id)->toHtml(),
            'class' => 'hidden'
        ), $parameters);
        $this->_addField($id, 'text', $label, $params);
    }

    protected function _addProductPicker($id, $label, $parameters = array())
    {
        $useMassaction = array_key_exists('use_massaction', $parameters) ? $parameters['use_massaction'] : true;
        $params = array_merge(array(
            'class' => 'hidden',
            'after_element_html' => Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setElementId($id)
                ->setUseMassaction($useMassaction)
                ->setTemplate('mzentrale/widget/form/renderer/product/picker.phtml')->toHtml()
        ), $parameters);
        $this->_addField($id . 'value', 'text', $label, $params);
    }

    protected function _addStaticBlockLink($id, $label, $parameters = array())
    {
        $params = array_merge(array(
            'class' => 'hidden',
            'after_element_html' => Mage::getSingleton('core/layout')->createBlock('core/template')
                ->setElementId($id)
                ->setTemplate('mzentrale/widget/form/renderer/cms/block/picker.phtml')->toHtml()
        ), $parameters);
        $this->_addField($id . 'value', 'text', $label, $params);
        $this->_addField($id . 'transport', 'hidden', '', array('required' => false));
    }

    /**
     * @return Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Buttons
     */
    public function getFormButtonsBlock()
    {
        return Mage::getSingleton('core/layout')->createBlock($this->_formButtonsBlock)->setTemplate($this->_formButtonsTemplate);
    }

    /**
     * @return Mage_Widget_Model_Widget_Instance|mixed
     */
    public function getWidgetInstance()
    {
        return Mage::registry('current_widget_instance');
    }

    protected function _addColumnSelect($id, $label, $parameters = array())
    {
        $params = array_merge(array(
            'values' => Mage::getSingleton('mzentrale_widget/source_columns')->toOptionArray()
        ), $parameters);
        $this->_addField($id, 'select', $label, $params);
    }

    protected function _addYesNoSelect($id, $label, $parameters = array())
    {
        $params = array_merge(array(
            'values' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray()
        ), $parameters);
        $this->_addField($id, 'select', $label, $params);
    }
}
