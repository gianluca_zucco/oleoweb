<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer_Copy extends Mzentrale_Widget_Block_Adminhtml_Widget_Instance_Grid_Renderer
{
    protected $_template = 'mzentrale/widget/instance/grid/renderer/copy.phtml';

    public function getStoresOptions()
    {
        return array_filter(Mage::app()->getStores(), array($this, 'filter'));
    }

    /**
     * @param Mage_Core_Model_Store $store
     * @return bool
     */
    public function filter($store)
    {
        return !in_array($store->getId(), $this->getWidgetInstance()->getStoreIds());
    }
}
