<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Date extends Mage_Adminhtml_Block_Widget_Grid
{
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        /** @var Varien_Data_Form $form */
        $form = $element->getForm();
        $params = $this->getWidgetInstance()->getWidgetParameters();
        preg_match('/\[(.*)\]/', $element->getName(), $matches);
        $value = array_key_exists(end($matches), $params) ? $params[end($matches)] : null;
        $element2 = new Varien_Data_Form_Element_Date(
            array(
                'name' => $element->getName(),
                'label' => 'Date',
                'after_element_html' => sprintf('<small>%s</small>', Mage::helper('mzentrale_widget')->__('Click to select')),
                'tabindex' => 1,
                'type'  => 'datetime',
                'false'=> true,
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'format' => 'yyyy-MM-dd',
                'value' => $value ? $value : date(
                    'yyyy-MM-dd',
                    strtotime('next weekday')
                )
            )
        );

        $element->setName(Mage::helper('core')->uniqHash())->setHtmlId(Mage::helper('core')->uniqHash());

        $element2->setForm($form);
        $element2->setId('date'.$element->getHtmlId());
        $html = $element2->getElementHtml();
        $html .= $this->_addHideOriginalFieldJs($element);
        $element->setData('after_element_html', $html);
        return $element2;
    }

    protected function _addHideOriginalFieldJs(Varien_Data_Form_Element_Abstract $element)
    {
        return sprintf('<script type="text/javascript">$("%s").hide()</script>', $element->getHtmlId());
    }

    /**
     * @return Mage_Widget_Model_Widget_Instance|mixed
     */
    public function getWidgetInstance()
    {
        return Mage::registry('current_widget_instance');
    }
}
