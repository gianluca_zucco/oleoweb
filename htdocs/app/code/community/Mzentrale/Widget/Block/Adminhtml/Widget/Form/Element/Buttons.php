<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Element_Buttons extends Mage_Adminhtml_Block_Template
{
    /**
     * @param $suffix
     * @return string
     */
    public function getWysiwygUrl($suffix)
    {
        return $this->getUrl('*/cms_wysiwyg_images/index/', array(
            'target_element_id' => '#{field_prefix}_' . $suffix
        ));
    }
}
