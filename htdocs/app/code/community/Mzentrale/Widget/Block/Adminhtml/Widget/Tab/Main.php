<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Widget
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Widget Instance Main tab block
 *
 * @category    Mage
 * @package     Mage_Widget
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mzentrale_Widget_Block_Adminhtml_Widget_Tab_Main
    extends Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Main
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Main
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        if (!Mage::app()->isSingleStoreMode()) {
            /** @var Varien_Data_Form_Element_Fieldset $fieldset */
            $fieldset = $this->getForm()->getElement('base_fieldset');
            $fieldset->removeField('store_ids');
            $fieldset->addField('store_ids', 'select', array(
                'name'      => 'store_ids[]',
                'label'     => Mage::helper('widget')->__('Assign to Store Views'),
                'title'     => Mage::helper('widget')->__('Assign to Store Views'),
                'required'  => true,
                'values'    => Mage::getSingleton('mzentrale_widget/source_store')->toOptionGroupArray(),
            ));
        }

        return $this;
    }
}
