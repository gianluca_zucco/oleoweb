<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 *
 * @method $this setTitle()
 * @method string getTitle()
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Form_Renderer_Divider extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
{
    protected function _construct()
    {
        $this->setTemplate('mzentrale/widget/form/renderer/divider.phtml');
    }

    /**
     * Getter
     *
     * @return Mage_Widget_Model_Widget_Instance
     */
    public function getWidgetInstance()
    {
        return Mage::registry('current_widget_instance');
    }
}
