<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Block_Adminhtml_Widget_Page extends Mzentrale_Widget_Block_Adminhtml_Widget_Abstract
{
    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Page layout');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Page layout settings');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return string
     */
    protected function _getButtonLabel()
    {
        return $this->__('Add row');
    }

    public function getAvailableElements()
    {
        return Mage::helper('mzentrale_widget')->getConfiguredPageAvailableElements();
    }

    /**
     * Returns renderer class
     *
     * @return string
     */
    public function getRenderer()
    {
        return Mage::getSingleton('core/layout')->createBlock('mzentrale_widget/adminhtml_widget_form_renderer_page');
    }
}
