<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

abstract class Mzentrale_Widget_Block_Product extends Mzentrale_Widget_Block_Abstract
{
    /**
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getPriceHtml(Mage_Catalog_Model_Product $product)
    {
        /** @var Mage_Catalog_Block_Product_Price $renderer */
        $renderer = $this->getChild('price_renderer');
        return $renderer->getPriceHtml($product);
    }

    public function getReviewSummaryHtml(Mage_Catalog_Model_Product $product)
    {
        /** @var Mage_Review_Block_Helper $renderer */
        $renderer = $this->getChild('reviews_renderer');
        return $renderer->getSummaryHtml($product, 'short', true);
    }

    protected function _prepareLayout()
    {
        $block = $this->getLayout()->createBlock('catalog/product_price');
        $this->setChild('price_renderer', $block);
        if (Mage::helper('catalog')->isModuleEnabled('Mage_Review')) {
            $this->setChild('reviews_renderer', $this->getLayout()->createBlock('review/helper'));
        }
        return parent::_prepareLayout();
    }
}
