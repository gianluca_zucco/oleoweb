<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project orsay
 */

class Mzentrale_Widget_Model_Instance extends Mage_Widget_Model_Widget_Instance
{
    const WIDGET_CUSTOM_FIELDNAME = 'childs';
    /**
     * Generate layout update xml
     *
     * @param string $blockReference
     * @param string $templatePath
     * @return string
     */
    public function generateLayoutUpdateXml($blockReference, $templatePath = '')
    {
        if (!$this->getWidgetConfig()->getAttribute('config')) {
            return parent::generateLayoutUpdateXml($blockReference, $templatePath);
        }
        $templateFilename = Mage::getSingleton('core/design_package')->getTemplateFilename($templatePath, array(
            '_area'    => $this->getArea(),
            '_package' => $this->getPackage(),
            '_theme'   => $this->getTheme()
        ));

        if (!$this->getId() && !$this->isCompleteToCreate()
            || ($templatePath && !is_readable($templateFilename)))
        {
            return '';
        }

        $parameters = $this->getWidgetParameters();
        $template = array_key_exists('template', $parameters) ? $parameters['template'] : '';
        if (!$template) {
            $template = !$this->getWidgetConfig()->parameters->template ? '' : (string) $this->getWidgetConfig()->parameters->template->value;
        }
        unset($parameters[$template]);

        if (!array_key_exists(self::WIDGET_CUSTOM_FIELDNAME, $parameters))    {
            return '';
        }
        $xml = $this->_generateBasicBlockXml($this->getType(), $template);
        foreach ($parameters[self::WIDGET_CUSTOM_FIELDNAME] as $key => $childData)  {
            $xml->appendChild($this->_generateChildBlockXml($childData, $key));
        }
        unset($parameters[self::WIDGET_CUSTOM_FIELDNAME]);

        foreach ($parameters as $name => $value) {

            if (is_array($value)) {
                $value = implode(',', $value);
            }
            if ($name && strlen((string)$value)) {
                $action = $this->_generateActionXml($name, $value);
                $xml->appendChild($action);
            }
        }

        $reference = new Varien_Simplexml_Element('<reference></reference>');
        $reference->addAttribute('name', $blockReference);
        $reference->appendChild($xml);
        $domxml = dom_import_simplexml($reference);
        return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
    }

    protected function _generateChildBlockXml(array $data, $key, $parentKey = null)
    {
        if (!$parentKey)    {
            $key = preg_replace('/_[\d]+/', '', $key);
            $type = Mage::getStoreConfig(sprintf('%s/%s/frontend_block', $this->_getConfigBasePath(), $key));
            $template = Mage::getStoreConfig(sprintf('%s/%s/template', $this->_getConfigBasePath(), $key));
            $xml = $this->_generateBasicBlockXml($type, $template);
        } else {
            $type = Mage::getStoreConfig(sprintf('%s/%s/children/%s/frontend_block', $this->_getConfigBasePath(), $parentKey, $key));
            $columns = Mage::getStoreConfig(sprintf('%s/%s/children/%s/columns', $this->_getConfigBasePath(), $parentKey, $key));
            $template = Mage::getStoreConfig(sprintf('%s/%s/children/%s/template', $this->_getConfigBasePath(), $parentKey, $key));
            $data['columns'] = $columns;
            $xml = $this->_generateBasicBlockXml($type, $template);
        }

        foreach ($data as $k => $v) {

            if ($k == self::WIDGET_CUSTOM_FIELDNAME)    {
                foreach ($data[$k] as $childKey => $childData)  {
                    $xml->appendChild($this->_generateChildBlockXml($childData, $childKey, $key));
                }
                continue;
            }

            if ($v == '') continue; //0 is allowed
            $k = $this->_translateKey($key, $k);
            $xml->appendChild($this->_generateActionXml($k, $v));
        }

        return $xml;
    }

    /**
     * @return string
     */
    protected function _getConfigBasePath()
    {
        return sprintf('mzentrale_widget/%s', $this->getWidgetConfig()->getAttribute('config'));
    }

    /**
     * @param $templatePath
     * @param $type
     * @return Varien_Simplexml_Element
     */
    protected function _generateBasicBlockXml($type, $templatePath = null)
    {
        $xml = new Varien_Simplexml_Element('<block></block>');
        $xml->addAttribute('type', $type);
        $xml->addAttribute('name', Mage::helper('core')->uniqHash());
        if ($templatePath)  {
            $xml->addAttribute('template', $templatePath);
        }
        return $xml;
    }

    /**
     * @param $name
     * @param $value
     * @return Varien_Simplexml_Element
     */
    protected function _generateActionXml($name, $value)
    {
        $action = new Varien_Simplexml_Element('<action></action>');
        $method = sprintf('set%s', uc_words($name, ''));
        $action->addAttribute('method', $method);
        $action->addChild('value', strpos($name, 'html') === false ? Mage::helper('widget')->escapeHtml($this->_getActionValue($value)) : $this->_getActionValue($value));
        return $action;
    }

    /**
     * @param $key
     * @param $k
     * @return mixed
     */
    protected function _translateKey($key, $k)
    {
        //Keys should be translated, since they are generated in several tricky way to fit in a single form
        $pattern = sprintf('/%1$s$|%1$svalue$|value$/', $key);
        if (preg_match($pattern, $k)) {
            $k = preg_replace($pattern, '', $k);
            return $k;
        }
        return $k;
    }

    /**
     * @param $v
     * @return string
     */
    protected function _getActionValue($v)
    {
        return is_array($v) ? implode(',', $v) : $v;
    }
}
