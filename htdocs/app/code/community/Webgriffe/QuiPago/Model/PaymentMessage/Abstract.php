<?php
/**
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setMerchantAlias(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setImporto(float $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setCodTrans(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setCustomerEmail(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setBrowserReturnUrl(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setSessionId(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setBrowserCancelUrl(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setLanguageId(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setServerReturnUrl(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setMac(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Abstract setEncryptionMethod(string $value)
 *
 * @method string getMerchantAlias()
 * @method float getImporto()
 * @method string getCodTrans()
 * @method string getCustomerEmail()
 * @method string getBrowserReturnUrl()
 * @method string getSessionId()
 * @method string getBrowserCancelUrl()
 * @method string getLanguageId()
 * @method string getServerReturnUrl()
 * @method string getMac()
 * @method string getEncryptionMethod()
 */
abstract class Webgriffe_QuiPago_Model_PaymentMessage_Abstract extends Varien_Object
{
    /**
     * Merchant Alias di test
     */
    const ALIAS_TEST   = 'payment_testm_urlmac';

    /**
     * Chiave di test per il calcolo del MAC
     */
    const ENCKEY_TEST   = 'esempiodicalcolomac';

    /**
     * La lingua di default per il frontend del portale di pagamento. 
     */
    const LANG_DEFAULT  = 'ITA';

    /**
     * La parte iniziale del prefisso di TEST
     */
    const TEST_PREFIX_PREFIX = 'WG-';

    /**
     * L'array delle lingua supportate dal portale.
     */
    protected $_languages = array(
        'it' => 'ITA',
        'en' => 'ENG',
        'es' => 'SPA',
        'fr' => 'FRA',
        'de' => 'GER',
        'ja' => 'JPN',
        'zh' => 'CHI',
        'ar' => 'ARA',
        'ru' => 'RUS',
    );

    /**
     * L'array di parametri da inviare o ricevere.
     * 
     * @var array
     */
    protected $_params = null;
    
    /**
     * La composizione della stringa da encodare dipende
     * messaggio di richiesta di pagamento o ricevendo un messaggio di esito.
     */
    abstract public function getStringToEncode();
    
    /**
     * La composizione dell'array di parametri dipende  se stiamo inviando un
     * messaggio di richiesta di pagamento o ricevendo un messaggio di esito.
     */
    abstract public function getParams();

    /**
     * L'importo è espresso in centesimi di euro senza sparatore, i primi 2 
     * numeri a destra rappresentano i centesimi di euro; 
     * es.: 5000 corrisponde a 50,00
     * 
     * In modalità di test l'importo è sempre corrispondente a Euro 1,00 .
     * 
     * @return string 
     */
    public function getImportoFormatted()
    {
        // TODO legge il valore in data e lo riformatta
        if ($this->getIsTest()) {
            return '100';
        }
        return (string) round($this->getData('importo') * 100);
    }
    
    public function getDivisa()
    {
        // Al momento sono supportati solo pagamenti in Euro
        return 'EUR';
    }

    public function getCodTransFormatted()
    {
        if ($this->getIsTest()) {
            return $this->_getTestPrefix() . $this->getData('cod_trans');
        }
        return $this->getData('cod_trans');
    }
    
    public function getDecodedCodTrans($codTrans = null)
    {
        if (is_null($codTrans)) {
            $codTrans = $this->getCodTrans();
        }
        
        if (substr($codTrans, 0, strlen(self::TEST_PREFIX_PREFIX)) == self::TEST_PREFIX_PREFIX) {
            return substr($codTrans, strlen($this->_getTestPrefix()));
        }
        return $this->getData('cod_trans');
    }
    
    public function getCalculatedMac()
    {
        Mage::helper('wgquipago')->log(
            'Called getCalculatedMac() with method '.$this->getEncryptionMethod().' on string: "' . $this->getStringToEncode() . '"'
        );
        $mac = Mage::helper('wgquipago')->calculateMac($this->getStringToEncode(), $this->getEncryptionMethod());
        Mage::helper('wgquipago')->log('Calculated Mac: ' . $mac);
        return $mac;
    }
    
    protected function _getTestPrefix()
    {
        return sprintf('WG-%08d-', time() % 100000000);
    }
}
