<?php

/**
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setDataTrans(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setOrarioTrans(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setEsito(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setCodAut(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setBrand(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setNome(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setCognome(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setEmail(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setNazionalita(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setPan(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage_Response setScadenzaPan(string $value)
 * @method string getDataTrans()
 * @method string getOrarioTrans()
 * @method string getEsito()
 * @method string getCodAut()
 * @method string getBrand()
 * @method string getNome()
 * @method string getCognome()
 * @method string getEmail()
 * @method string getNazionalita()
 * @method string getPan()
 * @method string getScadenzaPan()
 */
class Webgriffe_QuiPago_Model_PaymentMessage_Response extends Webgriffe_QuiPago_Model_PaymentMessage_Abstract
{
    const RESULT_OK = 'OK';
    const RESULT_KO = 'KO';

    public function getParams()
    {
        if (is_null($this->_params) || $this->hasDataChanges()) {
            $this->_params = array();
            $this->_params['session_id'] = $this->getSessionId();
            $this->_params['codAut'] = $this->getCodAut();
            $this->_params['alias'] = $this->getMerchantAlias();
            $this->_params['orario'] = $this->getOrarioTrans();
            $this->_params['data'] = $this->getDataTrans();
            $this->_params['mac'] = $this->getMac();
            $this->_params['importo'] = $this->getImportoFormatted();
            $this->_params['$BRAND'] = $this->getBrand();
            $this->_params['cognome'] = $this->getCognome();
            $this->_params['nazionalita'] = $this->getNazionalita();
            $this->_params['pan'] = $this->getPan();
            $this->_params['divisa'] = $this->getDivisa();
            $this->_params['email'] = $this->getCustomerEmail();
            $this->_params['scadenza_pan'] = $this->getScadenzaPan();
            $this->_params['esito'] = $this->getEsito();
            $this->_params['codTrans'] = $this->getCodTrans();
            $this->_params['nome'] = $this->getNome();
            $this->_params['messaggio'] = $this->getMessaggio();

            Mage::helper('wgquipago')->log('The following PaymentMessage_Response parameters were initialized:');
            Mage::helper('wgquipago')->log(print_r($this->_params, true));
        }
        return $this->_params;
    }

    public function getStringToEncode()
    {
        Mage::dispatchEvent(
            'webgriffe_quipago_get_response_before',
            array('response' => $this)
        );

        $string = 'codTrans=' . $this->getCodTrans()
                . 'esito=' . $this->getEsito()
                . 'importo=' . $this->getImportoFormatted()
                . 'divisa=' . $this->getDivisa()
                . 'data=' . $this->getDataTrans()
                . 'orario=' . $this->getOrarioTrans()
                . 'codAut=' . $this->getCodAut()
                . $this->getMacKey();

        $stringWrapper = new Varien_Object(array('string' => $string));
        Mage::dispatchEvent(
            'webgriffe_quipago_get_response_after',
            array('string' => $stringWrapper, 'response' => $this)
        );

        return $stringWrapper->getData('string');
    }

    public function initialize(Mage_Core_Controller_Request_Http $request)
    {
        Mage::helper('wgquipago')->log('PaymentMessage_Response initialized with the following parameters:');
        Mage::helper('wgquipago')->log(print_r($request->getParams(), true));

        // First of all, get the Order which generated the Response; this way
        // we can recover proper configuration.
        #$this->setCodTrans($this->_decodeCodTrans($request->getParam('codTrans')));
        $this->setCodTrans($request->getParam('codTrans'));
        $incrementId = $this->getDecodedCodTrans();

        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
        if ($order->getId()) {
            $storeId = $order->getStoreId();
        } else {
            throw new Exception('Unrecognized Increment Id: ' . $incrementId);
        }

        //
        // Set configuration based values
        //
        $configData = Mage::getStoreConfig(
            'payment/' . Webgriffe_QuiPago_Model_PaymentMethod::PAYMENT_METHOD_CODE,
            $storeId
        );
        $this->setIsTest($configData['test_mode']);
        $this->setEncryptionMethod($configData['mac_encryption']);
        $this->setMacKey($configData['mac_key']);

        //
        // Set other Request based values
        //
        $this->setSessionId($request->getParam('session_id'));
        $this->setCodAut($request->getParam('codAut'), 'NO-COD-AUT');
        $this->setMerchantAlias($request->getParam('alias'));
        $this->setOrarioTrans($request->getParam('orario'));
        $this->setDataTrans($request->getParam('data'));
        $this->setMac($request->getParam('mac'));
        $this->setImporto((float) $request->getParam('importo', 0) / 100);
        $this->setBrand($request->getParam('$BRAND'));
        $this->setCognome($request->getParam('cognome'));
        $this->setNazionalita($request->getParam('nazionalita'));
        $this->setPan($request->getParam('pan'));
        $this->setDivisa($request->getParam('divisa'));
        $this->setCustomerEmail($request->getParam('email'));
        $this->setScadenzaPan($request->getParam('scadenza_pan'));
        $this->setEsito($request->getParam('esito'));
        $this->setNome($request->getParam('nome'));
        $this->setMessaggio($request->getParam('messaggio'));

        return true;
    }

    public function isSuccessful()
    {
        return $this->getEsito() == self::RESULT_OK;
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if ($this->_order == null) {
            $incrementId = $this->getDecodedCodTrans();
            Mage::helper('wgquipago')->log("Decoded Order Increment Id is: '%s'", $incrementId);
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
            if ($this->_order->getId()) {
                // Arricchisce l'entità con tutti i dati
                $this->_order = $this->_order->load($this->_order->getId());
            } else {
                Mage::helper('wgquipago')->log("Couldn't retrieve Order by Increment Id '%s'", $incrementId);
            }
        }
        return $this->_order;
    }

    public function getOrderId()
    {
        if ($this->getOrder()) {
            return $this->getOrder()->getId();
        }
        return '';
    }

    public function handleRequest(
        Mage_Core_Controller_Request_Http $request,
        $isServerToServer = false,
        $dontCheckMac = false
    ) {
        $isSuccessful = false;

        $this->initialize($request);
        if ($this->isNotValidAfterInitialization($dontCheckMac)) {
            Mage::helper('wgquipago')->log('Invalid Payment Response.');
        } elseif ($this->isNotAlreadyHandled($isServerToServer)) {
            $this->updateOrderState();
            $this->createOrderTransaction();

            if ($this->isSuccessful()) {
                //Intervenire su Esito per impostare se questa transazione, per qualche motivo, non deve essere
                //considerata completata con successo
                Mage::dispatchEvent('webgriffe_quipago_payment_succesful', array('response' => $this));
            }

            if ($this->isSuccessful()) {
                Mage::helper('wgquipago')->log('Successful Payment for Order ' . $this->getOrderIncrementId());
                $isSuccessful = true;
            } else {
                Mage::helper('wgquipago')->log('Unsuccessful Payment for Order ' . $this->getOrderIncrementId());
            }
        } else {
            Mage::dispatchEvent('webgriffe_quipago_order_already_paid', array('response' => $this));
            Mage::helper('wgquipago')->log('Payment for Order ' . $this->getOrderIncrementId() . ' already handled.');
            /** @var Webgriffe_QuiPago_Model_PaymentMethod $paymentMethod */
            $paymentMethod = $this->getOrder()->getPayment()->getMethodInstance();
            $successfulState = $paymentMethod->getStateAndStatusFromConfig('order_success_status');
            $isSuccessful = !strcmp($successfulState['state'], $this->getOrder()->getState());
        }
        return $isSuccessful;
    }

    protected function isNotValidAfterInitialization($dontCheckMac = false)
    {
        $differentMacs = false;

        if ($dontCheckMac) { // used only for "cancel" action by User on KeyClient
            Mage::helper('wgquipago')->log("Incoming MAC is empty, avoiding check");
        } else {
            Mage::helper('wgquipago')->log('Incoming MAC: ' . $this->getMac());
            Mage::helper('wgquipago')->log('Calculated MAC: ' . $this->getCalculatedMac());

            if ($differentMacs = $this->getMac() != $this->getCalculatedMac()) {
                Mage::helper('wgquipago')->log('Incoming MAC is different from Calculated MAC.');
            }
        }

        $order = $this->getOrder();

        return $differentMacs || !$order;
    }

    protected function isNotAlreadyHandled($isServerToServer)
    {
        #if (!$isServerToServer) return true;

        /** @var Webgriffe_QuiPago_Model_PaymentMethod $paymentMethod */
        $paymentMethod = $this->getOrder()->getPayment()->getMethodInstance();
        $newOrderState = $paymentMethod->getStateAndStatusFromConfig('order_new_status');

        return ($this->getOrder()->getState() == $newOrderState['state']);
    }

    protected function updateOrderState()
    {
        /** @var Webgriffe_QuiPago_Model_PaymentMethod $paymentMethod */
        $paymentMethod = $this->getOrder()->getPayment()->getMethodInstance();

        if ($this->isSuccessful()) {
            $state = $paymentMethod->getStateAndStatusFromConfig('order_success_status');

            if ($paymentMethod->isSendEmailOnPayment()) {
                $this->getOrder()->sendNewOrderEmail();
            }
            if ($paymentMethod->isCreateInvoiceOnPayment()) {
                $order = $this->getOrder();
                $invoice = $order->prepareInvoice()->register();
                $order->addRelatedObject($invoice);
            }
        } else {
            $state = $paymentMethod->getStateAndStatusFromConfig('order_fault_status');
        }

        try {
            $order = $this->getOrder();
            $oldState = $order->getState();

            if (!strcmp(Mage_Sales_Model_Order::STATE_CANCELED, $state['state'])) {
                $order->cancel()->save();
                Mage::helper('wgquipago')->log('Order ' . $this->getOrder()->getIncrementId() . ' canceled');
            } else {
                $order->setState($state['state'], $state['status'], $this->toHtmlString())->save();
                Mage::helper('wgquipago')->log(
                    sprintf(
                        "State set to '%s' and status to '%s' for Order with IncrementId '%d'",
                        $state['state'],
                        $state['status'],
                        $this->getOrder()->getIncrementId()
                    )
                );
            }

            Mage::dispatchEvent(
                'webgriffe_quipago_update_order_state_after',
                array('order' => $order, 'old_state' => $oldState, 'response' => $this)
            );
        } catch (Exception $orderSaveException) {
            Mage::helper('wgquipago')->log($orderSaveException->getMessage());
        }
    }

    protected function createOrderTransaction()
    {
        Mage::helper('wgquipago')->log('Called createOrderTransaction()');
        
        if (Mage::helper('wgquipago')->isOldVersion()) {
            Mage::helper('wgquipago')->log(
                'Transaction can\'t be created on current Magento version: '. Mage::getVersion()
            );
            return;
        }
        
        $order = $this->getOrder();
        $transaction = $this->getTransaction($order);
        try {
            $transaction = $transaction
                ->setIsClosed($this->isSuccessful())
                ->setTxnType(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER)
                ->save();

            Mage::dispatchEvent(
                'webgriffe_quipago_create_transaction_after',
                array('transaction' => $transaction, 'order' => $order, 'response' => $this)
            );

            Mage::helper('wgquipago')->log('Transaction was created with id: ' . $transaction->getId());
        } catch (Exception $ex) {
            Mage::helper('wgquipago')->log($ex->getMessage());
        }
    }

    /**
     * @return string
     */
    protected function getOrderIncrementId()
    {
        return $this->getDecodedCodTrans();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order_Payment_Transaction|null
     */
    protected function getTransaction(Mage_Sales_Model_Order $order)
    {
        $transaction = null;
        $helper = Mage::helper('wgquipago');
        $payment = $order->getPayment();
        if ($payment->getId()) {
            $transaction = Mage::getModel('sales/order_payment_transaction')
                ->setOrderId($order->getId())
                ->setPaymentId($payment->getId())
                ->setTxnId($this->_getTransactionId())
                ->setOrderPaymentObject($payment)
                ->setAdditionalInformation(
                    Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,
                    $this->toTransactionArray()
                )->setCreatedAt(
                    $helper->decodePayDate($this->getDataTrans()) . ' ' .
                    $helper->decodePayTime($this->getOrarioTrans(), ':')
                );
        }
        return $transaction;
    }

    /**
     * @return string
     */
    protected function _getTransactionId()
    {
        return $this->getCodAut() . '-' . date('YmdHis');
    }

    /**
     * @return array
     */
    protected function toTransactionArray()
    {
        $array = array();
        $helper = Mage::helper('wgquipago');
        foreach ($this->getParams() as $key => $value) {
            $array[$helper->__($key)] = $value;
        }
        return $array;
    }

    /**
     * @return string
     */
    protected function toHtmlString()
    {
        $html = '';
        foreach ($this->getParams() as $key => $value) {
            $html .= $key . ': <strong>' . $value . '</strong><br/>';
        }
        return $html;
    }
}
