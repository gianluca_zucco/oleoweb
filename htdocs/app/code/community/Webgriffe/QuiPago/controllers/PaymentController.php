<?php

class Webgriffe_QuiPago_PaymentController extends Mage_Core_Controller_Front_Action
{
    /**
     * Default Helper
     */
    protected $_helper;

    /**
     * Costruttore
     * 
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs 
     */
    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = array()
    ) {
        parent::__construct($request, $response, $invokeArgs);
        $this->_helper = Mage::helper('wgquipago');
    }

    /**
     * Funzione di logging
     * 
     * @param string $msg 
     */
    protected function _log($msg)
    {
        $this->_helper->log($msg);
    }

    /*
     * Action per la funzionalità di Ripeti Pagamento
     */
    public function repayAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()) {
                Mage::dispatchEvent('webgriffe_quipago_repay_action', array('order' => $order));

                $this->_helper->getCheckoutSession()->setLastRealOrderId($order->getIncrementId());

                /** @var Webgriffe_QuiPago_Model_PaymentMethod $paymentMethod */
                $paymentMethod = $order->getPayment()->getMethodInstance();
                $paymentMethod
                        ->getInfoInstance()
                        ->setAdditionalInformation('isRepay', true)
                        ->save();

                return $this->_redirectUrl($paymentMethod->getOrderPlaceRedirectUrl());
            }
        }
        return false;
    }

    /*
     * Action per la funzionalità di Pagamento
     */
    public function requestAction()
    {
        $checkoutSession = $this->_helper->getCheckoutSession();

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($checkoutSession->getLastRealOrderId());
        if (!$order->getId()) {
            Mage::throwException('No order found with Increment Id ' . $checkoutSession->getLastRealOrderId());
        }

        /** @var Webgriffe_QuiPago_Model_PaymentMethod $method */
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $isRepay = $method->getInfoInstance()->getAdditionalInformation('isRepay');

        try {
            if ($quote = $checkoutSession->getQuote()) {
                $quote->setIsActive(false)->save();
            }

            $paymentRequestMsg = Mage::getModel('wgquipago/paymentMessage_request');
            $paymentRequestMsg->initialize($order, $isRepay);

            $checkoutSession->setQuoteId(null);

            $htmlForm = $paymentRequestMsg->getHtmlForm();
            Mage::log($htmlForm, null, 'Webgriffe_QuiPago-' . date('Ymd-His-u') . '.html');
            $this->getResponse()->setBody($htmlForm);
        } catch (Exception $e) {
            $this->_log($e->getMessage());
            if ($isRepay) {
                return $this->_redirect('sales/order/view/order_id/' . $order->getId() . '/');
            } else {
                return $this->_redirect('checkout/cart');
            }
        }
    }

    /**
     * Gestisce il ritorno al Checkout dopo un tentativo di pagamento sul portale Key Client.
     */
    public function returnCheckoutAction()
    {
        $this->_log('Called returnCheckoutAction()');
        $this->_log('Request params: '.print_r($this->getRequest()->getParams(), true));

        /** @var Webgriffe_QuiPago_Model_PaymentMessage_Response $paymentResponseMsg */
        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');
        $paymentResponseMsg->initialize($this->getRequest());

        Mage::dispatchEvent('webgriffe_quipago_return_checkout_action', array('response' => $paymentResponseMsg));

        if ($paymentResponseMsg->isSuccessful()) {
            //Non aggiornare lo stato dell'ordine qui. Bisogna farlo nella serverToServerAction
            Mage::dispatchEvent(
                'webgriffe_quipago_return_checkout_action_after',
                array('response' => $paymentResponseMsg)
            );
            return $this->_redirect('checkout/onepage/success');
        }

        Mage::dispatchEvent(
            'webgriffe_quipago_return_checkout_action_after_error',
            array('response' => $paymentResponseMsg)
        );
        return $this->_redirect('checkout/onepage/failure');
    }

    /**
     * Gestisce il ritorno al Checkout dopo la cancellazione del pagamento sul portale Key Client.
     */
    public function cancelCheckoutAction()
    {
        $this->_log('Called cancelCheckoutAction()');
        $this->_log('Request params: '.print_r($this->getRequest()->getParams(), true));

        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');

        Mage::dispatchEvent('webgriffe_quipago_cancel_checkout_action', array('response' => $paymentResponseMsg));

        //Niente mac tra i parametri inviati quando si annulla un pagamento...
        $paymentResponseMsg->handleRequest($this->getRequest(), false, true);

        $this->_helper->getCheckoutSession()->setErrorMessage($this->__('Payment canceled.'));

        Mage::dispatchEvent('webgriffe_quipago_cancel_checkout_action_after_error');
        return $this->_redirect('checkout/onepage/failure');
    }

    /**
     * Gestisce il ritorno all'Ordine dopo un tentativo di pagamento sul portale Key Client.
     */
    public function returnRepayAction()
    {
        $this->_log('Called returnRepayAction()');
        $this->_log('Request params: '.print_r($this->getRequest()->getParams(), true));

        /** @var Webgriffe_QuiPago_Model_PaymentMessage_Response $paymentResponseMsg */
        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');
        $paymentResponseMsg->initialize($this->getRequest());

        Mage::dispatchEvent('webgriffe_quipago_return_repay_action', array('response' => $paymentResponseMsg));

        if ($paymentResponseMsg->isSuccessful()) {
            Mage::dispatchEvent(
                'webgriffe_quipago_return_repay_action_after',
                array('response' => $paymentResponseMsg)
            );
        } else {
            Mage::dispatchEvent(
                'webgriffe_quipago_return_repay_action_after_error',
                array('response' => $paymentResponseMsg)
            );
        }
        $this->_redirect('sales/order/view/order_id/' . $paymentResponseMsg->getOrderId());
    }

    /**
     * Gestisce il ritorno all'Ordine dopo la cancellazione del pagamento sul portale Key Client.
     */
    public function cancelRepayAction()
    {
        $this->_log('Called cancelRepayAction()');
        $this->_log('Request params: '.print_r($this->getRequest()->getParams(), true));

        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');

        Mage::dispatchEvent('webgriffe_quipago_cancel_repay_action', array('response' => $paymentResponseMsg));

        $paymentResponseMsg->handleRequest($this->getRequest());

        $this->_helper->getCoreSession()->addError($this->__('Payment canceled.'));
        Mage::dispatchEvent('webgriffe_quipago_cancel_repay_action_after', array('response' => $paymentResponseMsg));

        $this->_redirect('sales/order/view/order_id/' . $paymentResponseMsg->getOrderId());
    }

    /**
     * Gestisce la notifica Server To Server da Key Client a Magento relativamente ad un pagamento effettuato.
     */
    public function serverToServerAction()
    {
        $this->_log('Called serverToServerAction()');
        $this->_log('Request params: '.print_r($this->getRequest()->getParams(), true));

        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');
        $paymentResponseMsg->handleRequest($this->getRequest(), true);
    }
}
