<?php
/**
 * Created by PhpStorm.
 * User: ddonnini
 * Date: 31/12/14
 * Time: 20.21
 */

class Webgriffe_QuiPago_Test_Model_PaymentMessage_Request extends EcomDev_PHPUnit_Test_Case
{
    public function encryptionMethods()
    {
        return array(
            array(Webgriffe_QuiPago_Helper_Data::ENCTYPE_SHA1, '992e40c00b79ad1a6e4a5a8c61e776e696796a79'),
            array(Webgriffe_QuiPago_Helper_Data::ENCTYPE_MD5, 'ZjRkZDdkNWNmYThlZmYyNTJiN2U1ZmI2MDJlNjM5NDI%3D'),
        );
    }

    /**
     * @dataProvider encryptionMethods
     */
    public function testCalculateMac($method, $expectedResult)
    {
        $paymentMsg = Mage::getModel('wgquipago/paymentMessage_request');
        $paymentMsg
            ->setMacKey(Webgriffe_QuiPago_Model_PaymentMessage_Abstract::ENCKEY_TEST)
            ->setCodTrans('testCILME534')
            ->setDivisa('EUR')
            ->setImporto(0.01)
            ->setEncryptionMethod($method);

        $this->assertEquals(
            $expectedResult,
            $paymentMsg->getCalculatedMac(),
            "Il calcolo del MAC con il metodo $method è errato"
        );
    }

    public function testHtmlFormGeneration()
    {
        $paymentMsg = Mage::getModel('wgquipago/paymentMessage_request');
        $paymentMsg
            ->setMacKey(Webgriffe_QuiPago_Model_PaymentMessage_Abstract::ENCKEY_TEST)
            ->setMerchantAlias(Webgriffe_QuiPago_Model_PaymentMessage_Abstract::ALIAS_TEST)
            ->setCodTrans('testCILME534')
            ->setCustomerEmail('mail@webgriffe.com')
            ->setBrowserReturnUrl('noreturnurl')
            ->setBrowserCancelUrl('nocancelurl')
            ->setServerReturnUrl('noserverurl')
            ->setSessionId('nosessionid')
            ->setLanguageId('ITA')
            ->setImporto(0.01)
            ->setDivisa('EUR')
            ->setMac('992e40c00b79ad1a6e4a5a8c61e776e696796a79')
            ->setEncryptionMethod(Webgriffe_QuiPago_Helper_Data::ENCTYPE_SHA1);

        $this->assertEquals(
            '2c873446749f33b49ba52db89e85da4b',
            md5($paymentMsg->getHtmlForm()),
            "La firma del Form generato è cambiata"
        );
    }

    public function testInitialize()
    {
        $this->mockSession('core/session');
        $session = $this->getHelperMock('wgquipago', array('getCurrentSessionId'));
        $session
            ->expects($this->once())
            ->method('getCurrentSessionId')
            ->will($this->returnValue('gaergaer78g'));
        $this->replaceByMock('helper', 'wgquipago', $session);

        $order = new Mage_Sales_Model_Order();
        $order->setStoreId(1);
        $order->setBaseCurrencyCode('EUR');
        $order->setBaseGrandTotal(0.01);
        $order->setIncrementId('testCILME534');
        $order->setCustomerEmail('mail@webgriffe.com');

        $paymentMsg = Mage::getModel('wgquipago/paymentMessage_request');
        $paymentMsg->initialize($order);

        // Forza alcuni valori che la initialize imposta da Config in modo da
        // avere una situazione dell'oggetto nota
        $paymentMsg->setIsTest(false);
        $paymentMsg->setEncryptionMethod(Webgriffe_QuiPago_Helper_Data::ENCTYPE_SHA1);
        $paymentMsg->setMacKey(Webgriffe_QuiPago_Model_PaymentMessage_Abstract::ENCKEY_TEST);

        $this->assertEquals(
            '992e40c00b79ad1a6e4a5a8c61e776e696796a79',
            $paymentMsg->getCalculatedMac(),
            "Wrong Mac code after initialization of Payment Request Message"
        );
    }

    public function testEncodeDecodeCodTrans()
    {
        $paymentMsg = Mage::getModel('wgquipago/paymentMessage_request');
        $paymentMsg->setIsTest(true);
        $paymentMsg->setCodTrans('100000000001');
        $codFormatted = $paymentMsg->getCodTransFormatted();

        $this->assertEquals(
            '100000000001',
            $paymentMsg->getDecodedCodTrans($codFormatted),
            "Encoded and Decoded Cod Trans differ"
        );
    }
}
