<?php
/**
 * Created by PhpStorm.
 * User: ddonnini
 * Date: 31/12/14
 * Time: 20.21
 */

class Webgriffe_QuiPago_Test_Model_PaymentMessage_Response extends EcomDev_PHPUnit_Test_Case
{
    public function encryptionMethods()
    {
        return array(
            array('sha1', 'd8614a933b63486417245fc313f810068ceb5804'),
            array('md5', 'M2EyNWFjNTU5NzEwYTNlOTJiNjk1ZjQ1NjUwMWY2Yzk%3D'),
        );
    }

    /**
     * @dataProvider encryptionMethods
     */
    public function testCalculateMac($method, $expectedResult)
    {
        $paymentMsg = Mage::getModel('wgquipago/paymentMessage_response')
            ->setMacKey(Webgriffe_QuiPago_Model_PaymentMessage_Abstract::ENCKEY_TEST)
            ->setCodTrans('ORD_01')
            ->setEsito('OK')
            ->setImporto(0.1)
            ->setDivisa('EUR')
            ->setDataTrans('20110616')
            ->setOrarioTrans('174003')
            ->setCodAut('TESTOK')
            ->setEncryptionMethod($method);

        $this->assertEquals(
            $expectedResult,
            $paymentMsg->getCalculatedMac(),
            "Il calcolo del MAC con il metodo $method è errato"
        );
    }
}
