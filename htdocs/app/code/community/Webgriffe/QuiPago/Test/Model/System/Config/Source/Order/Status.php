<?php
/**
 * @author Manuele Menozzi <mmenozzi@webgriffe.com>
 */

class Webgriffe_QuiPago_Test_Model_System_Config_Source_Order_Status extends EcomDev_PHPUnit_Test_Case
{
    public function testToOptionArrayReturnsStateAndStatus()
    {
        $statusSource = Mage::getModel('wgquipago/system_config_source_order_status');
        $return = $statusSource->toOptionArray();

        $this->assertEquals(
            array(
                array('label' => '-- Please Select --', 'value' => ''),
                array(
                    'label' => 'New',
                    'value' => array(
                        array(
                            'label' => 'Pending',
                            'value' => 'new::pending'
                        )
                    )
                ),
                array (
                    'label' => 'Pending Payment',
                    'value' => array(
                        array(
                            'label' => 'Pending Payment',
                            'value' => 'pending_payment::pending_payment',
                        )
                    )
                ),
                array (
                    'label' => 'Processing',
                    'value' => array(
                        array(
                            'label' => 'Processing',
                            'value' => 'processing::processing',
                        )
                    )
                ),
                array (
                    'label' => 'Canceled',
                    'value' => array(
                        array(
                            'label' => 'Canceled',
                            'value' => 'canceled::canceled',
                        )
                    )
                ),
                array (
                    'label' => 'On Hold',
                    'value' => array(
                        array(
                            'label' => 'On Hold',
                            'value' => 'holded::holded',
                        )
                    )
                ),
            ),
            $return
        );
    }
} 