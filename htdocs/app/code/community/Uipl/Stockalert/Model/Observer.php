<?php

class Uipl_Stockalert_Model_Observer
{

    public function getalert(Varien_Event_Observer $observer)
    {
        if (Mage::getStoreConfigFlag('general/stockalert/active')) {

            /** @var Mage_Sales_Model_Order $order */
            $order = $observer->getEvent()->getOrder();
            $ordered_items = $order->getAllItems();

            /** @var Mage_Sales_Model_Order_Item $item */
            foreach ($ordered_items as $item) {

                /** @var Mage_CatalogInventory_Model_Stock_Item $stockItem */
                $_product = $item->getProduct();
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
                if (!$stockItem->getUseConfigNotifyStockQty()) continue;
                $notifyAt = $stockItem->getNotifyStockQty();

                if ($stockItem->getQty() <= $notifyAt) {
                    $email_to = Mage::getStoreConfig('trans_email/ident_sales/email');
                    $email_to .= "," . Mage::getStoreConfig('general/stockalert/otheremails');
                    $emailTemplate = Mage::getModel('core/email_template')
                        ->loadDefault('outofstock_email_template');


                    $expro = '<table width="100%" cellpadding="10" cellspacing="10">
                                    <tr><td>Product Id</td><td>Product Name</td><td>SKU</td><td>Product Url</td></tr>
                                ';

                    $expro .= '<tr><td>' . $_product->getId() . '</td><td>' . $_product->getName() . '</td><td>' . $_product->getSku() . '</td><td>' . $_product->getProductUrl() . '</td></tr>';


                    $expro .= '</table>';

                    $email_template_variables = array(
                        'alertGrid' => $expro,


                    );


                    $sender_name = Mage::getStoreConfig('trans_email/ident_general/name');;

                    $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');

                    $emailTemplate->setSenderName($sender_name);
                    $emailTemplate->setSenderEmail($sender_email);
                    $emailTemplate->getProcessedTemplate($email_template_variables);
                    $emailTemplate->send($email_to, 'Product Low Stock Notification', $email_template_variables);
                }
            }
        }
    }
}

?>
