function switchFields(countryCode) {

    var attribute_code_hide = 'vat_id';
    var attribute_code_show = 'tax_id';
    if (countryCode != 'IT') {
        attribute_code_hide = 'tax_id';
        attribute_code_show = 'vat_id';
    }

    if (typeof countryCode == 'boolean') {
        if (countryCode) {
            attribute_code_hide = 'tax_id';
            attribute_code_show = 'vat_id';
        }

        if (!countryCode && getCountryCode() == 'IT') {
            attribute_code_hide = 'vat_id';
            attribute_code_show = 'tax_id';
        }
    }

    $$(['li', attribute_code_hide].join('.')).each(function (elm) {
        elm.down('input').removeClassName('required-entry');
        var label = elm.down('label');
        if (label.innerHTML.indexOf('<em>*</em>') != -1) {
            label.removeClassName('required').update(label.innerHTML.substr(0, label.innerHTML.indexOf('<em>*</em>')-1));
        }
    }).invoke('hide');
    $$(['li', attribute_code_show].join('.')).each(function (elm) {
        elm.down('input').addClassName('required-entry');
        var label = elm.down('label');
        if (label.innerHTML.indexOf('<em>*</em>') == -1) {
            label.addClassName('required').update([label.innerText, '<em>*</em>'].join(' '));
        }
    }).invoke('show');
}

function getCountryCode() {
    return $('country') ? $F('country') : $F('billing:country_id');
}

$(document).observe('dom:loaded', function() {
    try {
        var country = getCountryCode();
        switchFields(country);
        if ($('billing:country_id')) {
            $('billing:country_id').observe('change', function () {
                switchFields($F(this));
            });
        }
        if ($('country')) {
            $('country').observe('change', function () {
                switchFields($F(this));
            });
        }
        if ($('business_account')) {
            switchFields($F('business_account'));
        }
    } catch (e) {
        //empty on purpose
    }
});
