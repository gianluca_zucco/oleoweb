#!/usr/bin/env bash

cd "$(pwd)/htdocs/skin/frontend/kosmosol/default";

if [ $1 ]; then

    gulp $1

else

    gulp

fi

cd -;
